/**
 * @file common.c
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of STM32DriverPack
 *
 * STM32DriverPack is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * STM32DriverPack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

#include "common.h"

void DelayUS(uint32_t us) {
		uint32_t multiplier = CLOCK_FREQUENCY_HZ / 4000000;
	    us = us * multiplier;
	    while (us--);
}

void GPIO_clk_enable(GPIO_TypeDef* gpio) {
	if (gpio == GPIOA) {
		__HAL_RCC_GPIOA_CLK_ENABLE();
		return;
	}
	if (gpio == GPIOB) {
		__HAL_RCC_GPIOB_CLK_ENABLE();
		return;
	}
	if (gpio == GPIOC) {
		__HAL_RCC_GPIOC_CLK_ENABLE();
		return;
	}
	if (gpio == GPIOD) {
		__HAL_RCC_GPIOD_CLK_ENABLE();
		return;
	}
	if (gpio == GPIOE) {
		__HAL_RCC_GPIOE_CLK_ENABLE();
		return;
	}
}
