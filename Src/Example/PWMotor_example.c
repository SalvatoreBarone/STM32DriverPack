/**
 * @file PWMotor_example.c
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of STM32DriverPack
 *
 * STM32DriverPack is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * STM32DriverPack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
 
#include "stm32f4xx_hal.h"
#include "PWMotor.h"

void SystemClock_Config(void);

int main(void) {
	HAL_Init();
	SystemClock_Config();

	PWMotor_t pwm4;
	
	// Solo per due su quattro dei canali, in questo esempio, è prevista la possibilità di cambiare
	// il senso di rotazione. Il senso di rotazione del canale 1 sarà comandato tramite PC2, mentre
	// il senso di rotazione del canale 2 sarà comandato tramite PC3.
	PortPinPair_t gpios[4] = {
			{.Port = GPIOC, .Pin = GPIO_PIN_2},
			{.Port = GPIOC, .Pin = GPIO_PIN_3},
			{.Port = 0, .Pin = 0},
			{.Port = 0, .Pin = 0}
	};

	// Inizializzazione del generatore:
	// il bus APB è cloccato a 42 MHz, per cui il valore del parametro apb_tim_freq_hz
	// viene impostato a 80M. Il segnale PWM generato avrà frequenza di 1 HZ.	
	PWMotor_Init(&pwm4, PWM_4, gpios, 84000000, 1);

	// Impostazione del duty-cycle.
	// Il duty-cycle dei diversi canali può essere impostato in modo indipendete.
	// I canali vengono impostati affinché il duty-cycle sia, rispettivamente, del
	// 10%, del 30%, del 50% e 70%
	PWMotor_SetDutyCycle(&pwm4, PWM_ch_1, 10, polarity_low);
	PWMotor_SetDutyCycle(&pwm4, PWM_ch_2, 30, polarity_low);
	PWMotor_SetDutyCycle(&pwm4, PWM_ch_3, 50, polarity_low);
	PWMotor_SetDutyCycle(&pwm4, PWM_ch_4, 70, polarity_low);

	// Avvio del processo di generazione del segnale PWM
	PWMotor_Start(&pwm4, PWM_ch_1);
	PWMotor_Start(&pwm4, PWM_ch_2);
	PWMotor_Start(&pwm4, PWM_ch_3);
	PWMotor_Start(&pwm4, PWM_ch_4);

	for (;;);

}

void SystemClock_Config(void) {

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	__HAL_RCC_PWR_CLK_ENABLE();
	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 4;
	RCC_OscInitStruct.PLL.PLLN = 168;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = 7;
	HAL_RCC_OscConfig(&RCC_OscInitStruct);

	RCC_ClkInitStruct.ClockType = 	RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK |
									RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
	HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5);

	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);
	HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}
