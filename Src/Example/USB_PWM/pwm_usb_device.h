/**
 * @file pwm_usb_device.h
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of STM32DriverPack
 *
 * STM32DriverPack is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * STM32DriverPack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

#ifndef __PWM_USB_DEVICE_H__
#define __PWM_USB_DEVICE_H__

#include "PWMotor.h"

void pwm_action(PWMotor_t *pwmGen_ptr, uint8_t* Buf, uint32_t *Len);

extern PWMotor_t *pwmGen_ptr;

#endif
