/**
 * @file host.c
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of STM32DriverPack
 *
 * STM32DriverPack is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * STM32DriverPack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <fcntl.h>


// La struttura seguente contiene i parametri che è possibile passare al programma quando esso viene avviato
typedef struct 
{
	char* dev_filename;	// nome del file (in /dev) col quale interagire
	uint8_t* data;		// dati da inviare al device
	uint8_t data_size;	// dimensione dei dati (numero di byte)
} param_t;

// la funzione effettua il parsing degli argomenti passati al programma e imposta i campi della
// struttura param_t
int parse_args(int argc, char **argv, param_t *param);

int main(int argc, char **argv) 
{
	param_t param;
	int descriptor;	// descrittore del file
	
	param.dev_filename = NULL;
	param.data = NULL;
	param.data_size = 0;
	
	// parsing degli argomenti
	if (parse_args(argc, argv, &param) != 0)
		return -1;
		
	// apertura del file specificato, in modalità lettura/scrittura
	descriptor = open(param.dev_filename, O_RDWR);
	if (descriptor < 1) 
	{
		printf("Errore durante l'apertura del file\n");
		return -1;
	}
	// invio dei dati al device
	write(descriptor, param.data, param.data_size);
	// chiusura del file
	close(descriptor);
	return 0;
} 

int parse_args(int argc, char **argv, param_t *param) 
{
	int par;
	
	// parsing degli argomenti
	// Il parsing viene effettuato usando la funzione getopt().
	// 
	// #include <unistd.h>
	// int getopt(int argc, char const argv[], const charoptstring);
	// 
	// Essa prende in input i parametri argc ed argv passati alla funzione main() quando il programma viene invocato.
	// Quando una delle stringhe che compongono argv comincia con il carattere '-', getopt() la considera una opzione.
	// Il carattere immediatamente successivo il '-' identifica la particolare opzione.
	// La funzione può essere chiamata ripetutamente, fino a quando non restituisce -1, ad indicare che sono stati
	// analizzati tutti i parametri passati al programma.
	// Quando getopt() trova un'opzione, restituisce quel carattere ed aggiorna la variabile globale optind, che punta
	// al prossimo parametro contenuto in argv.
	// La stringa optstring indica quali sono le opzioni considerate. Se una opzione è seguita da ':' vuol dire che
	// essa è seguita da un argomento. Tale argomento può essere ottenuto mediante la variabile globale optarg.
	while((par = getopt(argc, argv, "d:w:")) != -1) 
	{
		switch (par) 
		{
		// l'opzione 'd' è seguita dal path del file corrispondente al device col quale interagire
		case 'd' :
			param->dev_filename = optarg;
			break;
		// l'opzione 'w' è seguita dai dati che si intende spedire al device
		case 'w' :
			param->data = (uint8_t*) optarg;
			param->data_size = strlen(optarg);
			break;
		// ogni altra opzione non riconosciuta causerà la terminazione del programma
		default :
			printf("%c: parametro sconosciuto.\n", par);
			return -1;
		}
	}
	
	// viene verificato che sia stato specificato il device col quale interagire
	if (param->dev_filename == NULL) 
	{
		printf("E' necessario specificare un device col quale interagire\n");
		return -1;
	}
	
	// viene verificato che siano stati specificati i dati da inviare al suddetto device
	if (param->data == NULL || param->data_size == 0) 
	{
		printf("E' necessario specificare i dati da spedire al device\n");
		return -1;
	}

	return 0;
}
 
