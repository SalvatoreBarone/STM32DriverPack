/**
 * @file pwm_usb_device.c
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of STM32DriverPack
 *
 * STM32DriverPack is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * STM32DriverPack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

#include "pwm_usb_device.h"

void pwm_action(PWMotor_t *pwmGen_ptr_ptr, uint8_t* Buf, uint32_t *Len) {
	uint8_t i;
	PWMotor_Ch_t current_ch = PWM_ch_1;

	// Viene scandito tutto il buffer ricevuto.
	// Il puntatore Buf rappresenta il buffer di ricezione USB, il puntatore Len, invece, contiene
	// il numero di byte ricevuti.
	// Usando questo sistema è possibile impostare il duty-cycle di tutti i canali usando una stringa
	// opportuna. Ad esempio, ricevendo la stringa "a9b2c4ds", come sarà evidenziato nel seguito,
	// verranno impostati i duty cycle per i quattro canali del generatore ed avviata, o fermata, la
	// generazione del segnale.
	for (i = 0; i < *Len; i++) {
		switch (Buf[i]) {
		// Quando viene ricevuto un carattere tra {'a', 'b', 'c', 'd'} viene selezionato uno dei
		// quattro canali PWM di cui il generatore dispone.
		case 'a':
			current_ch = PWM_ch_1;
			break;
		case 'b':
			current_ch = PWM_ch_2;
			break;
		case 'c':
			current_ch = PWM_ch_3;
			break;
		case 'd':	
			current_ch = PWM_ch_4;
			break;	
			
		// Quando viene ricevuto 's' la generazione del segnale PWM viene arrestata
		case 's':
			PWMotor_Stop(pwmGen_ptr, current_ch);
			break;
			
		// Quando viene ricevuto 'f' il senso di rotazione viene posto a PWMotor_forward
		case 'f':
			PWMotor_SetDirection(pwmGen_ptr, current_ch, PWMotor_forward);
			break;

		// Quando viene ricevuto 'r' il senso di rotazione viene posto a PWMotor_reverse
		case 'r':
			PWMotor_SetDirection(pwmGen_ptr, current_ch, PWMotor_reverse);
			break;

		// Quando viene ricevuto 't' il senso di rotazione viene invertito
		case 't':
			PWMotor_ToggleDirection(pwmGen_ptr, current_ch);
			break;

		// Quando, invece, viene ricevuto un carattere appartenente all'insieme 
		// {'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'} viene impostato il duty cycle.
		// Ricevendo '1' verrà impostato un duty cycle del 10%, ricevendo '2' il duty cycle sarà del
		// 20%, e così via.
		case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
			PWMotor_SetDutyCycle(pwmGen_ptr, current_ch, (Buf[i]-'0')*10, polarity_high);
			PWMotor_Start(pwmGen_ptr, current_ch);
			break;
		// Lo '0' fa si che venga impostato un duty-cycle del 100%.
		case '0':
			PWMotor_SetDutyCycle(pwmGen_ptr, current_ch, 100, polarity_high);
			PWMotor_Start(pwmGen_ptr, current_ch);
			break;
		default:
			break;
		}
	}
}


