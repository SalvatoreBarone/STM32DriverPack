/**
 * @file PWMotor.c
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of STM32DriverPack
 *
 * STM32DriverPack is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * STM32DriverPack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */


#include "PWMotor.h"

/**
 * @brief Alias per le definizioni dei canali
 */
uint32_t PWMotor_TIM_Ch[] = {
		TIM_CHANNEL_1,	// channel 1
		TIM_CHANNEL_2,	// channel 2
		TIM_CHANNEL_3,	// channel 3
		TIM_CHANNEL_4	// channel 4
};

/**
 * @brief Base address dei TIM per la generazione dei segnali
 */
TIM_TypeDef* PWMotor_TIM_BaseAddr[] = {
		TIM2,	// PWM_2 -> TIM2
		TIM3,	// PWM_2 -> TIM3
		TIM4,	// PWM_4 -> TIM4
};

/**
 * @brief GPIO connessi ai canali di uscita dei generatori PWM
 *
 * Ognuno dei timer supportati dal modulo PWMotor possiede quattro canali, ognuno dei quali connesso ad un
 * GPIO. Tale GPIO costituisce il mezzo attraverso il quale il segnale PWM viene portato verso l'esterno.
 * La corrispondenza tra canale e particolare GPIO è riportata nella tabella seguente.
 *
 * <h4>STM32F3 Discovery (STM32F303VCTx)</h4>
 * <table>
 * <tr><th>Gen.</th><th>ch. 1</th><th>ch. 2</th><th>ch. 3</th><th>ch. 4</th></tr>
 * <tr><td>PWM_2 (TIM2)</td><td>PA0</td><td>PA1</td><td>PA2</td><td>PA3</td></tr>
 * <tr><td>PWM_3 (TIM3)</td><td>PE2</td><td>PE3</td><td>PE4</td><td>PE5</td></tr>
 * <tr><td>PWM_4 (TIM4)</td><td>PD12</td><td>PD13</td><td>PD14</td><td>PD15</td></tr>
 * </table>
 *
 * <h4>STM32F4 Discovery (STM32F407VGTx)</h4>
 * <table>
 * <tr><th>Gen.</th><th>ch. 1</th><th>ch. 2</th><th>ch. 3</th><th>ch. 4</th></tr>
 * <tr><td>PWM_2 (TIM2)</td><td>PA0</td><td>PA1</td><td>PA2</td><td>PA3</td></tr>
 * <tr><td>PWM_3 (TIM3)</td><td>PA6</td><td>PA7</td><td>PB0</td><td>PB1</td></tr>
 * <tr><td>PWM_4 (TIM4)</td><td>PD12</td><td>PD13</td><td>PD14</td><td>PD15</td></tr>
 * </table>
 *
 */
PortPinPair_t PWMotor_GPIO_PortPair[3][4] = {
	// PWM_2 -> TIM2
	{
		{.Port=GPIOA, .Pin=GPIO_PIN_0}, // coppia port-pin ch. 1
		{.Port=GPIOA, .Pin=GPIO_PIN_1}, // coppia port-pin ch. 2
		{.Port=GPIOA, .Pin=GPIO_PIN_2}, // coppia port-pin ch. 3
		{.Port=GPIOA, .Pin=GPIO_PIN_3}  // coppia port-pin ch. 4
	},
	// PWM_3 -> TIM3
	{
#if defined(STM32F303VCTx) || defined(STM32F3DISCOVERY) || defined(STM32F303xC)
		{.Port=GPIOE, .Pin=GPIO_PIN_2}, // coppia port-pin ch. 1
		{.Port=GPIOE, .Pin=GPIO_PIN_3}, // coppia port-pin ch. 2
		{.Port=GPIOE, .Pin=GPIO_PIN_4}, // coppia port-pin ch. 3
		{.Port=GPIOE, .Pin=GPIO_PIN_5}  // coppia port-pin ch. 4
#endif
#if defined(DSTM32F407VGTx) || defined(STM32F4DISCOVERY)
		{.Port=GPIOA, .Pin=GPIO_PIN_6}, // coppia port-pin ch. 1
		{.Port=GPIOA, .Pin=GPIO_PIN_7}, // coppia port-pin ch. 2
		{.Port=GPIOB, .Pin=GPIO_PIN_0}, // coppia port-pin ch. 3
		{.Port=GPIOB, .Pin=GPIO_PIN_1}  // coppia port-pin ch. 4
#endif
	},
	// PWM_4 -> TIM4
	{
		{.Port=GPIOD, .Pin=GPIO_PIN_12}, // coppia port-pin ch. 1
		{.Port=GPIOD, .Pin=GPIO_PIN_13}, // coppia port-pin ch. 2
		{.Port=GPIOD, .Pin=GPIO_PIN_14}, // coppia port-pin ch. 3
		{.Port=GPIOD, .Pin=GPIO_PIN_15}  // coppia port-pin ch. 4
	}
};

/**
 * @brief Alias per le alternate function con cui configurare i GPIO affinché fungano da output per il segnale PWM.
 */
uint32_t PWMotor_GPIO_Alternate[] = {
		GPIO_AF1_TIM2,	// PWM_2
		GPIO_AF2_TIM3,	// PWM_3
		GPIO_AF2_TIM4	// PWM_4
};

/**
 * @brief Configura ed abilita il clock per il generatore di segnale PWM
 *
 * @param [inout]	pwm 	puntatore a PWMotor_t, opportunamente inizializzato, che indica il particolare
 * 							generatore da usare.

 */
static void PWMotor_LL_Init(PWMotor_t* pwm) {
	switch (pwm->tim) {
	case PWM_2 :
		__HAL_RCC_TIM2_CLK_ENABLE();
		break;
	case PWM_3 :
		__HAL_RCC_TIM3_CLK_ENABLE();
		break;
	case PWM_4 :
		__HAL_RCC_TIM4_CLK_ENABLE();
		break;
	}
}

/**
 * @brief Configura i GPIO usati da ciascuno dei canali del generatore di segnale PWM
 *
 * @param [inout]	pwm 	puntatore a PWMotor_t, opportunamente inizializzato, che indica il particolare
 * 							generatore da usare.
 *
 */
static void PWMotor_GPIO_Init(PWMotor_t* pwm) {
	GPIO_InitTypeDef GPIO_InitStruct;
	uint8_t i;

	switch (pwm->tim) {
	case PWM_2 :
		__HAL_RCC_GPIOA_CLK_ENABLE();
		break;
	case PWM_3 :
#if defined(STM32F303VCTx) || defined(STM32F3DISCOVERY) || defined(STM32F303xC)
		__HAL_RCC_GPIOAE_CLK_ENABLE();
#endif
#if defined(DSTM32F407VGTx) || defined(STM32F4DISCOVERY)
		__HAL_RCC_GPIOA_CLK_ENABLE();
		__HAL_RCC_GPIOB_CLK_ENABLE();
#endif
		break;
	case PWM_4 :
		__HAL_RCC_GPIOD_CLK_ENABLE();
		break;
	}

	// configurazione dei GPIO PWM
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Alternate = PWMotor_GPIO_Alternate[pwm->tim];

	for (i=0; i<4; i++)
		if (PWMotor_GPIO_PortPair[pwm->tim][i].Port != NULL) {
			GPIO_InitStruct.Pin = PWMotor_GPIO_PortPair[pwm->tim][i].Pin;
			HAL_GPIO_Init(PWMotor_GPIO_PortPair[pwm->tim][i].Port, &GPIO_InitStruct);
		}

	// configurazione dei GPIO per la direzione
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Alternate = 0;
	for (i=0; i<4; i++)
		if (pwm->gpio_dir[i].Port != NULL && pwm->gpio_dir[i].Port != 0) {
			GPIO_InitStruct.Pin = pwm->gpio_dir[i].Pin;
			HAL_GPIO_Init(pwm->gpio_dir[i].Port, &GPIO_InitStruct);
			GPIO_clk_enable(pwm->gpio_dir[i].Port);
		}
}

/**
 * @brief Inizializza un generatore di segnale PWM.
 *
 * @param [inout]	pwm				puntatore a PWMotor_t da inizializzare
 * @param [in] 		generator		maschera di selezione del generatore di segnale PWM
 * @param [in]		gpio_dir 		array di quattro oggetti PortPinPair_t, ciascuno dei quali indica il particolare
 * 									GPIO assegnato al comando del senso di rotazione dei motori, uno per ciascun
 * 									canale di cui il generatore è dotato. I canali non usati possono essere
 * 									contrassegnati con un puntatore NULL o posto a "0".
 * @param [in]		apb_tim_freq_hz	frequenza di clock del bus APB a cui il generatore è connesso, moltiplicata per
 * 									due
 * @param [in]		frequency_hz 	frequenza del segnale PWM desiderata
 *
 * @details
 * Il generatore viene configurato in modo che l'incremento del conteggio segue il clock interno. La frequenza del
 * segnale PWM viene importata attraverso l'uso della funzione PWMotor_SetFrequency(). Inizialmente, il duty cycle di
 * ciascuno dei quattro canali è posto a zero, mentre la polarità del segnale a polarity_high. Il senso di rotazione
 * di default è PWMotor_forward.
 * La funzione si occupa anche della configurazione completa di ogni singolo device GPIO utilizzato del generatore,
 * sia che si tratti di un GPIO usato come veicolo per il segnale PWM, sia che si tratti di un GPIO usato per il
 * comando della direzione di rotazione del motore.
 */
void PWMotor_Init(	PWMotor_t* pwm,
					PWMotor_TIM_t generator,
					PortPinPair_t *gpio_dir,
					uint32_t apb_tim_freq_hz,
					uint32_t frequency_hz) {

	TIM_ClockConfigTypeDef sClockSourceConfig;
	TIM_MasterConfigTypeDef sMasterConfig;

	uint8_t i;

	pwm->tim = generator;
	PWMotor_LL_Init(pwm);

	pwm->TIMhandle.Instance = PWMotor_TIM_BaseAddr[pwm->tim];
	pwm->TIMhandle.Init.CounterMode = TIM_COUNTERMODE_UP;
	pwm->TIMhandle.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	PWMotor_SetFrequency(pwm, apb_tim_freq_hz, frequency_hz);

	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	HAL_TIM_ConfigClockSource(&pwm->TIMhandle, &sClockSourceConfig);
	HAL_TIM_PWM_Init(&pwm->TIMhandle);

	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	HAL_TIMEx_MasterConfigSynchronization(&pwm->TIMhandle, &sMasterConfig);

	for (i=0; i<4; i++) {
		PWMotor_SetDutyCycle(pwm, i, 0, polarity_high);
		pwm->gpio_dir[i].Port = gpio_dir[i].Port;
		pwm->gpio_dir[i].Pin = gpio_dir[i].Pin;
	}

	PWMotor_GPIO_Init(pwm);
	for (i=0; i<4; i++)
		PWMotor_SetDirection(pwm, i, PWMotor_forward);
}

/**
 * @brief Imposta la frequenza del segnale PWM.
 *
 * @param [inout]	pwm 			puntatore a PWMotor_t, opportunamente inizializzato, che indica il particolare
 * 									generatore da usare.
 * @param [in] 		apb_tim_freq_hz	frequenza di clock del bus APB a cui il generatore è connesso, moltiplicata per
 * 									due
 * @param [in] 		frequency_hz	frequenza del segnale PWM desiderata
 *
 * @details
 * Il valore di conteggio del timer usato per la generazione del segnale PWM viene incrementato sulla base del
 * segnale di clock del timer stesso. Tale segnale passa attraverso un prescaler che permette di abbassare la
 * frequenza con la quale il contatore viene incrementato. La frequenza di conteggio, espressa in Hz, è
 *
 * \f[
 * freq_{count}=\frac{freq_{APB}*2}{prescaler+1}
 * \f]
 *
 * La frequenza di incremento deo conteggio determina la frequenza del segnale PQM che verrà generato. Il valore
 * di conteggio massimo dipende dalla dimensione del registro del particolare timer usato. Tipicamente la dimensione
 * è 16 bit, per cui il valore massimo di conteggio è \f$ 2^{16}-1=65535\f$, giunti al quale il contatore viene
 * automaticamente resettato. La frequenza del segnale PWM può essere determinata in base alla seguente equazione:
 *
 * \f[
 * 	freq_{PWM}=\frac{freq_{count}}{period_{TIM}}+1
 * \f]
 *
 * dove \f$ period_{TIM}\f$ è il valore iniziale assunto dal registro contenente il valore di conteggio. Tipicamente
 * la frequenza del segnale PWM è nota, mentre il valore di \f$ period_{TIM}\f$ va calcolato. Allo scopo è possibile
 * usare
 *
 * \f[
 * period_{TIM}=\frac{freq_{count}}{freq_{PWM}}-1
 * \f]
 *
 * Espandendo \f$ freq_{count} \f$
 * \f[
 * period_{TIM}=\frac{freq_{APB}*2}{(prescaler+1)*freq_{PWM}}-1
 * \f]
 *
 * Se, ad esempio, si vuole generare un segnale PWM con frequenza 10KHz attraverso un timer a 16 bit, nel caso in
 * cui il bus APB è cloccato a 42MHz, impostando il prescaler a 0,
 *
 * \f[
 * period_{TIM}=\frac{84000000}{10000}-1=8399
 * \f]
 *
 * Nel caso in cui si ottenga un valore per \f$ period_{TIM} \f$ maggiore di \f$ 2^{16}-1=65535 \f$ sarà necessario
 * incrementare il valore di prescaler. La funzione imposta il timer proprio in questo modo: calcola il valore di
 * \f$ period_{TIM} \f$ incrementando gradualmente il valore del prescaler, partendo dal valore zero, cercando di
 * soddisfare la condizione \f$ period_{TIM}<65535 \f$.
 */
void PWMotor_SetFrequency(	PWMotor_t* pwm,
							uint32_t apb_tim_freq_hz,
							uint32_t frequency_hz) {
	uint8_t i;

	pwm->frequency = frequency_hz;
	pwm->TIMhandle.Init.Prescaler = 0;
	do
		pwm->TIMhandle.Init.Period = apb_tim_freq_hz / frequency_hz / (1 + pwm->TIMhandle.Init.Prescaler++);
	while (pwm->TIMhandle.Init.Period >= 65535);
	--pwm->TIMhandle.Init.Prescaler;
	HAL_TIM_Base_Init(&pwm->TIMhandle);

	for (i=0; i<4; i++)
		PWMotor_SetDutyCycle(pwm, i, pwm->duty_cycle[i], pwm->polarity[i]);
}

/**
 * @brief Imposta il duty-cycle del segnale PWM di uno dei canali di un generatore
 *
 * @param [inout]	pwm 	puntatore a PWMotor_t, opportunamente inizializzato, che indica il particolare generatore da usare.
 * @param [in]		channel	canale di cui si intende configurare il duty-cycle. Può essere
 * - PWM_ch_1, maschera di selezione del canale 1
 * - PWM_ch_2, maschera di selezione del canale 2
 * - PWM_ch_3, maschera di selezione del canale 3
 * - PWM_ch_4, maschera di selezione del canale 4
 * @param [in] duty_cycle 	duty-cycle desiderato, in percentuale
 * @param [in] polarity 	polarità del segnale PWM generato. Può essere
 * 	- polarity_low, il valore neutro del segnale PWM è zero logico
 * 	- polarity_high, il valore neutro del segnale PWM è uno logico
 *
 * @details
 * Ognuno dei timer supportati dal modulo PWMotor possiede quattro canali, i quali possono essere configurati
 * indipendentemente per generare segnali PWM con duty-cycle differente. La frequenza dei segnali, invece, è
 * unica per tutti i canali connessi ad uno stesso generatore PWMotor_TIM_t.
 * Il duty-cycle del segnale PWM viene gestito attraverso un valore di conteggio, detto "pulse length", superato il
 * quale il quale viene invertito il valore del segnale PWM. Il valore di duty-cycle in percentuale può essere
 * impostato usando
 * \f[
 * length_{pulse}=\frac{(period_{TIM}+1)*dutycicle}{100}-1
 * \f]
 *
 */
void PWMotor_SetDutyCycle(	PWMotor_t* pwm,
							PWMotor_Ch_t channel,
							uint8_t duty_cycle,
							PWMotor_Polarity_t polarity) {

	TIM_OC_InitTypeDef OCConfig;

	pwm->duty_cycle[channel] = (duty_cycle <= 100 ? duty_cycle : 100);
	pwm->polarity[channel] = polarity;

	OCConfig.OCMode = TIM_OCMODE_PWM1;
	OCConfig.OCPolarity = (polarity == polarity_high ? TIM_OCPOLARITY_HIGH : TIM_OCPOLARITY_LOW);
	OCConfig.OCFastMode = TIM_OCFAST_DISABLE;
	OCConfig.Pulse = ((pwm->TIMhandle.Init.Period+1)*pwm->duty_cycle[channel])/100-1;

	HAL_TIM_PWM_ConfigChannel(&pwm->TIMhandle, &OCConfig, PWMotor_TIM_Ch[channel]);
}

/**
 * @brief Avvia la generazione del segnale PWM.
 *
 * @param[in]	pwm		puntatore a PWMotor_t, opportunamente inizializzato, che indica il particolare generatore
 * 						da usare.
 * @param[in]	channel	canale su cui avviare la generazione del segnale PWM. Può essere
 * - PWM_ch_1, maschera di selezione del canale 1
 * - PWM_ch_2, maschera di selezione del canale 2
 * - PWM_ch_3, maschera di selezione del canale 3
 * - PWM_ch_4, maschera di selezione del canale 4
 */
void PWMotor_Start(PWMotor_t* pwm, PWMotor_Ch_t channel) {
	HAL_TIM_PWM_Start(&pwm->TIMhandle, PWMotor_TIM_Ch[channel]);
}

/**
 * @brief Ferma la generazione del segnale PWM.
 *
 * @param [in]	pwm		puntatore a PWMotor_t, opportunamente inizializzato, che indica il particolare generatore
 * 						da usare.
 * @param [in]	channel	canale su cui fermare la generazione del segnale PWM. Può essere
 * - PWM_ch_1, maschera di selezione del canale 1
 * - PWM_ch_2, maschera di selezione del canale 2
 * - PWM_ch_3, maschera di selezione del canale 3
 * - PWM_ch_4, maschera di selezione del canale 4
 */
void PWMotor_Stop(PWMotor_t* pwm, PWMotor_Ch_t channel) {
	HAL_TIM_PWM_Stop(&pwm->TIMhandle, PWMotor_TIM_Ch[channel]);
}

/**
 * @brief Imposta il senso di rotazione del motore connesso ad uno dei canali del generatore PWM
 *
 * @param [in]	pwm		puntatore a PWMotor_t, opportunamente inizializzato, che indica il particolare generatore
 * 						da usare.
 * @param [in]	channel	canale su cui impostare il senso di rotazione. Può essere
 * - PWM_ch_1, maschera di selezione del canale 1
 * - PWM_ch_2, maschera di selezione del canale 2
 * - PWM_ch_3, maschera di selezione del canale 3
 * - PWM_ch_4, maschera di selezione del canale 4
 * @param [in]	direction senso di rotazione. Può essere
 * - PWMotor_forward
 * - PWMotor_reverse
 */
void PWMotor_SetDirection(PWMotor_t* pwm, PWMotor_Ch_t channel, PWMotor_Direction_t direction) {
	if (pwm->gpio_dir[channel].Port != NULL && pwm->gpio_dir[channel].Port != 0)
		HAL_GPIO_WritePin(	pwm->gpio_dir[channel].Port,
							pwm->gpio_dir[channel].Pin,
							(direction == PWMotor_forward ? GPIO_PIN_RESET : GPIO_PIN_SET));
}

/**
 * @brief Inverte il senso di rotazione del motore connesso ad uno dei canali del generatore PWM
 *
 * @param [in]	pwm		puntatore a PWMotor_t, opportunamente inizializzato, che indica il particolare generatore
 * 						da usare.
 * @param [in]	channel	canale su cui invertire il senso di rotazione. Può essere
 * - PWM_ch_1, maschera di selezione del canale 1
 * - PWM_ch_2, maschera di selezione del canale 2
 * - PWM_ch_3, maschera di selezione del canale 3
 * - PWM_ch_4, maschera di selezione del canale 4
 */
void PWMotor_ToggleDirection(PWMotor_t* pwm, PWMotor_Ch_t channel) {
	if (pwm->gpio_dir[channel].Port != NULL && pwm->gpio_dir[channel].Port != 0)
		HAL_GPIO_TogglePin(	pwm->gpio_dir[channel].Port,
							pwm->gpio_dir[channel].Pin);
}




