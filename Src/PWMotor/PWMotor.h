/**
 * @file PWMotor.h
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of STM32DriverPack
 *
 * STM32DriverPack is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * STM32DriverPack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

/**
 * @addtogroup PWM
 * @{
 *
 * @defgroup PWMotor
 * @{
 * @brief Pilotaggio di un motore DC, sprovvisto di controllore PWM integrato, con PWM.
 *
 * Il modulo è pensato per gestire un generatore di segnale PWM nel suo insieme, quindi include le configurazioni
 * per ognuno dei quattro canali cui il generatore è provvisto, per il pilotaggio di un motore elettrico DC
 * sprovvisto di controllore PWM integrato.
 * Facendo uso dello schema elettrico riportato di seguito, il modulo consente di variare, da programma, il senso
 * e la velocità di rotazione del motore.
 *
 * @image html PWMotor_bjt.png
 * @image latex PWMotor_bjt.png
 *
 * Agendo sul segnale che, in figura, è indicato come "Rot", sarà possibile variare il senso di rotazione del
 * motore, mentre agendo sul segnale "Speed" sarà possibile variare la velocità di rotazione del motore.
 *
 * La frequenza del segnale PWM generato è la medesima per ognuno dei quattro canali di cui il generatore è provvisto.
 * Essa è configurabile sia in fase di inizializzazione, usando la funzione PWMotor_Init(), che successivamente,
 * usando la funzione PWMotor_SetFrequency().
 * Anche se la frequenza del segnale generato da ognuno dei canali è identica, il duty-cycle è configurabile
 * individualmente, usando la funzione PWMotor_SetDutyCycle().
 *
 * Una volta configurato il generatore, è possibile avviare o fermare la generazione del segnale, rispettivamente,
 * usando le funzioni PWMotor_Start() e PWMotor_Stop().
 *
 * @warning Di default, quando viene creato un nuovo progetto System Workbench, HAL_TIM_MODULE_ENABLED non è definita.
 * Tale macro abilita il modulo TIM della libreria HAL, necessario a poter usare il modulo PWMotor. Tipicamente questa
 * macro è definita in <b>stm32fxxx_hal_conf.h</b>, ma la definizione è commentata. <b>Per poter usare il modulo
 * PWMotor è necessario decommentare la definizione di tale macro</b>. Tipicamente si trova alla linea 84.
 */

#ifndef __PWM_MOTOR_CONTROL_H__
#define __PWM_MOTOR_CONTROL_H__

#include "common.h"

#if defined(STM32F30) || defined(STM32F3DISCOVERY) || defined(STM32F3) || defined(STM32F303VCTx) || defined(STM32F303xC)
#include "stm32f3xx_hal_tim.h"
#include "stm32f3xx_hal_tim_ex.h"
#endif

#if defined(DSTM32F407VGTx) || defined(STM32F4) || defined(STM32F4DISCOVERY) || defined(STM32F407xx)
#include "stm32f4xx_hal_tim.h"
#include "stm32f4xx_hal_tim_ex.h"
#endif

#include <inttypes.h>

/**
 * @brief Maschera di selezione del generatore di segnale PWM
 *
 * Il modulo supporta solo un sottoinsieme dei generatori PWM disponibili su STM32F3/F4, ossia quelli che
 * dispongono di quattro canali.
 * Ognuno dei canali di cui dispone un timer è connesso ad un GPIO. Tale GPIO costituisce il mezzo attraverso
 * il quale il segnale PWM viene portato verso l'esterno.
 * La corrispondenza tra canale e particolare GPIO è riportata nella tabella seguente.
 *
 * <h4>STM32F3 Discovery (STM32F303VCTx)</h4>
 * <table>
 * <tr><th>Gen.</th><th>ch. 1</th><th>ch. 2</th><th>ch. 3</th><th>ch. 4</th></tr>
 * <tr><td>PWM_2 (TIM2)</td><td>PA0</td><td>PA1</td><td>PA2</td><td>PA3</td></tr>
 * <tr><td>PWM_3 (TIM3)</td><td>PE2</td><td>PE3</td><td>PE4</td><td>PE5</td></tr>
 * <tr><td>PWM_4 (TIM4)</td><td>PD12</td><td>PD13</td><td>PD14</td><td>PD15</td></tr>
 * </table>
 *
 * <h4>STM32F4 Discovery (STM32F407VGTx)</h4>
 * <table>
 * <tr><th>Gen.</th><th>ch. 1</th><th>ch. 2</th><th>ch. 3</th><th>ch. 4</th></tr>
 * <tr><td>PWM_2 (TIM2)</td><td>PA0</td><td>PA1</td><td>PA2</td><td>PA3</td></tr>
 * <tr><td>PWM_3 (TIM3)</td><td>PA6</td><td>PA7</td><td>PB0</td><td>PB1</td></tr>
 * <tr><td>PWM_4 (TIM4)</td><td>PD12</td><td>PD13</td><td>PD14</td><td>PD15</td></tr>
 * </table>
 */
typedef enum {
	PWM_2,  //!< PWM_2, la sorgente del segnale PWM è TIM2
	PWM_3,  //!< PWM_3, la sorgente del segnale PWM è TIM3
	PWM_4   //!< PWM_4, la sorgente del segnale PWM è TIM4
} PWMotor_TIM_t;

/**
 * @brief Maschera di selezione del canale PWM
 *
 * Ognuno dei timer supportati dal modulo PWMotor possiede quattro canali, i quali possono essere configurati
 * indipendentemente per generare segnali PWM con duty-cycle differente. La frequenza dei segnali, invece, è
 * unica per tutti i canali connessi ad uno stesso generatore PWMotor_TIM_t.
 *
 * Ognuno dei canali di cui dispone un timer è connesso ad un GPIO. Tale GPIO costituisce il mezzo attraverso
 * il quale il segnale PWM viene portato verso l'esterno.
 * La corrispondenza tra canale e particolare GPIO è riportata nella tabella seguente.
 *
 * <h4>STM32F3 Discovery (STM32F303VCTx)</h4>
 * <table>
 * <tr><th>Gen.</th><th>ch. 1</th><th>ch. 2</th><th>ch. 3</th><th>ch. 4</th></tr>
 * <tr><td>PWM_2 (TIM2)</td><td>PA0</td><td>PA1</td><td>PA2</td><td>PA3</td></tr>
 * <tr><td>PWM_3 (TIM3)</td><td>PE2</td><td>PE3</td><td>PE4</td><td>PE5</td></tr>
 * <tr><td>PWM_4 (TIM4)</td><td>PD12</td><td>PD13</td><td>PD14</td><td>PD15</td></tr>
 * </table>
 *
 * <h4>STM32F4 Discovery (STM32F407VGTx)</h4>
 * <table>
 * <tr><th>Gen.</th><th>ch. 1</th><th>ch. 2</th><th>ch. 3</th><th>ch. 4</th></tr>
 * <tr><td>PWM_2 (TIM2)</td><td>PA0</td><td>PA1</td><td>PA2</td><td>PA3</td></tr>
 * <tr><td>PWM_3 (TIM3)</td><td>PA6</td><td>PA7</td><td>PB0</td><td>PB1</td></tr>
 * <tr><td>PWM_4 (TIM4)</td><td>PD12</td><td>PD13</td><td>PD14</td><td>PD15</td></tr>
 * </table>
 */
typedef enum {
	PWM_ch_1, //!< PWM_ch_1, maschera di selezione del canale 1
	PWM_ch_2, //!< PWM_ch_2, maschera di selezione del canale 2
	PWM_ch_3, //!< PWM_ch_3, maschera di selezione del canale 3
	PWM_ch_4  //!< PWM_ch_4, maschera di selezione del canale 4
} PWMotor_Ch_t;

/**
 * @brief Polarità del segnale PWM generato
 */
typedef enum {
	polarity_low,	//!< polarity_low, il livello attivo del segnale PWM è zero logico
	polarity_high	//!< polarity_high, il livello attivo del segnale PWM è uno logico
} PWMotor_Polarity_t;

/**
 * @brief Direzione di rotazione del motore.
 *
 * @note
 * La direzione di rotazione dipende anche dal modo in cui il motore è connesso al circuito
 * di pilotaggio riportato nella sezione descrittiva del modulo.
 */
typedef enum {
	PWMotor_forward,//!< PWMotor_forward
	PWMotor_reverse //!< PWMotor_reverse
} PWMotor_Direction_t;

/**
 * @brief Struttura che astrae un generatore di segnale PWM.
 *
 * La struttura è pensata per gestire un generatore di segnale PWM nel suo insieme, quindi include le configurazioni
 * per ognuno dei quattro canali cui il generatore è provvisto.
 * La frequenza del segnale PWM generato è la medesima per ognuno dei quattro canali di cui il generatore è provvisto.
 * Essa è configurabile sia in fase di inizializzazione, usando la funzione PWMotor_Init(), che successivamente,
 * usando la funzione PWMotor_SetFrequency().
 * Anche se la frequenza del segnale generato da ognuno dei canali è identica, il duty-cycle è configurabile
 * individualmente, usando la funzione PWMotor_SetDutyCycle().
 *
 * Una volta configurato il generatore, è possibile avviare o fermare la generazione del segnale, rispettivamente,
 * usando le funzioni PWMotor_Start() e PWMotor_Stop().
 */
typedef struct {
	TIM_HandleTypeDef TIMhandle;	/**< TIM Time Base Handle Structure */

	PWMotor_TIM_t tim;				/**< Istanza PWMotor_TIM_t che genererà il segnale PWM */

	PortPinPair_t gpio_dir[4];			/**< GPIO associato al controllo del senso di rotazione del motore */

	uint32_t frequency;				/**< Frequenza, in Hz, del segnale PWM generato. La frequenza del segnale PWM
										 generato è la medesima per ognuno dei quattro canali di cui il generatore
										 è provvisto. */

	uint8_t duty_cycle[4];			/**< Duty-cycle, in percentuale, del segnale PWM generato da ognuno dei
	 	 	 	 	 	 	 	 	 	 differenti canali */

	PWMotor_Polarity_t polarity[4];/**< Polarità del segnale PWM generato */

} PWMotor_t;


void PWMotor_Init(	PWMotor_t* pwm,
					PWMotor_TIM_t generator,
					PortPinPair_t *gpio_dir,
					uint32_t apb_tim_freq_hz,
					uint32_t frequency_hz);

void PWMotor_SetFrequency(	PWMotor_t* pwm,
							uint32_t apb_tim_freq_hz,
							uint32_t frequency_hz);

void PWMotor_SetDutyCycle(	PWMotor_t* pwm,
							PWMotor_Ch_t channel,
							uint8_t duty_cycle,
							PWMotor_Polarity_t polarity);

void PWMotor_Start(PWMotor_t* pwm, PWMotor_Ch_t channel);

void PWMotor_Stop(PWMotor_t* pwm, PWMotor_Ch_t channel);

void PWMotor_SetDirection(PWMotor_t* pwm, PWMotor_Ch_t channel, PWMotor_Direction_t direction);

void PWMotor_ToggleDirection(PWMotor_t* pwm, PWMotor_Ch_t channel);

/**
 * @example PWMotor_example.c
 * In questo esempio d'uso del modulo PWMotor viene mostrato come inizializzare un generatore PWM, come impostarne
 * la frequenza, come impostare il duty-cycle dei segnali generati da ognuno dei canali di cui il generatore è dotato.
 */ 

/**
 * @example USB_PWM/pwm_usb_device.c
 * Il file USB_PWM/pwm_usb_device.c contiene l'implementazione della funzione di esempio pwm_action().
 * Questa funzione effettua il parsing di una stringa stringa di byte ricevuta tramite USB (CDC)
 * per avviare/fermare la generazione del segnale PWM per uno specifico generatore, indicato
 * dal puntatore pwmGen. Tale funzione può essere inserita all'interno del corpo della funzione
 * CDC_Receive_FS(), tipicamente definita in "usbd_cdc_if.c", così come riportato di seguito.
 * 
 * @code
 * 	static int8_t CDC_Receive_FS (uint8_t* Buf, uint32_t *Len)
 * 	{
 *		pwm_action(pwmGen_ptr, Buf, Len);
 *		USBD_CDC_SetRxBuffer(&hUsbDeviceFS, &Buf[0]);
 *		USBD_CDC_ReceivePacket(&hUsbDeviceFS);
 *		return (USBD_OK);
 * 	}
 * @endcode
 * 
 * La funzione CDC_Receive_FS() non possiede un parametro puntatore a PWMotor_t, per cui sarà
 * necessario utilizzare una variabile extern.
 * <br><br>
 * Il file USB_PWM/pwm_usb_device.h contiene la dichiarazione della funzione di esempio pwm_action()
 * e la dichiarazione della variabile extern .
 * Il file "pwm_usb_device.h" andrà incluso in "usbd_cdc_if.c", dove è implementata la funzione
 * CDC_Receive_FS().
 * <br>
 * Segue il codice del file USB_PWM/pwm_usb_device.h.
 * @include  USB_PWM/device.h
 * Nel file main.c, dove è implementata la funzione main(), si dovrà dichiarare
 * @code
 *	PWMotor_t pwmGen;
 *	PWMotor_t *pwmGen_ptr = &pwmGen;
 * @endcode
 * Ovviamente pwmGen va inizializzato usando PWMotor_Init().
 * <br><br>
 * Il file USB_PWM/host.c contiene un programma di esempio, da eseguire lato host (ad esempio su un
 * computer a cui è connesso un device USB) che consente di inviare, usando la classe CDC, una
 * stringa di byte passati come parametro al programma, in modo che il device USB connesso al PC
 * possa usarli in qualche modo.
 * <br>
 * Segue il codice del file USB_PWM/host.c.
 * @include USB_PWM/host.c
 * <br><br><br>
 * Segue il codice del file USB_PWM/pwm_usb_device.c.
 */

#endif

/**
 * @}
 * @}
 */
