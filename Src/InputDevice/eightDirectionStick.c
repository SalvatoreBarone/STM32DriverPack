/**
 * @file eightDirectionStick.c
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of STM32DriverPack
 *
 * STM32DriverPack is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * STM32DriverPack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */



#include "eightDirectionStick.h"
#include <assert.h>

int eightDStick_ValidatePair(eightDStick_t* stick);

int eightDStick_ValidatePair_EXTI(eightDStick_t* stick);

uint8_t	eightDStick_GetPortState(eightDStick_t* stick);

void eightDStick_ConfigurePin(eightDStick_t* stick);

void eightDStick_ConfigurePin_EXTI(eightDStick_t* stick);

void eightDStick_Init(	eightDStick_t* stick,
						PortPinPair_t N,
						PortPinPair_t S,
						PortPinPair_t E,
						PortPinPair_t W)
{
	assert(stick);
	stick->N.Port = N.Port;
	stick->S.Port = S.Port;
	stick->E.Port = E.Port;
	stick->W.Port = W.Port;
	stick->N.Pin = N.Pin;
	stick->S.Pin = S.Pin;
	stick->E.Pin = E.Pin;
	stick->W.Pin = W.Pin;
	assert(eightDStick_ValidatePair(stick));
	eightDStick_ConfigurePin(stick);
}

void eightDStick_Init_EXTI(	eightDStick_t* stick,
							PortPinPair_t N,
							PortPinPair_t S,
							PortPinPair_t E,
							PortPinPair_t W,
							IRQn_Type IRQn,
							uint32_t PreemptPriorty,
							uint32_t SubPriority)
{
	assert(stick);
	assert(IRQn == EXTI_LINE_9_5 || IRQn == EXTI_LINE_15_10);
	stick->N.Port = N.Port;
	stick->S.Port = S.Port;
	stick->E.Port = E.Port;
	stick->W.Port = W.Port;
	stick->N.Pin = N.Pin;
	stick->S.Pin = S.Pin;
	stick->E.Pin = E.Pin;
	stick->W.Pin = W.Pin;
	assert(eightDStick_ValidatePair_EXTI(stick));
	eightDStick_ConfigurePin_EXTI(stick);

	stick->IRQn = IRQn;
	stick->PreemptPriorty = PreemptPriorty;
	stick->SubPriority = SubPriority;
	HAL_NVIC_SetPriority(stick->IRQn, stick->PreemptPriorty, stick->SubPriority);
	HAL_NVIC_EnableIRQ(stick->IRQn);

}

void eightDStick_Init_v2(	eightDStick_t* stick,
							GPIO_TypeDef* N_Port, uint16_t N_Pin,
							GPIO_TypeDef* S_Port, uint16_t S_Pin,
							GPIO_TypeDef* E_Port, uint16_t E_Pin,
							GPIO_TypeDef* W_Port, uint16_t W_Pin)
{
	PortPinPair_t N = {N_Port, N_Pin},
	S = {S_Port, S_Pin},
	E = {E_Port, E_Pin},
	W = {W_Port, W_Pin};
	eightDStick_Init(stick, N, S, E, W);
}

void eightDStick_Init_EXTI_v2(	eightDStick_t* stick,
								GPIO_TypeDef* N_Port, uint16_t N_Pin,
								GPIO_TypeDef* S_Port, uint16_t S_Pin,
								GPIO_TypeDef* E_Port, uint16_t E_Pin,
								GPIO_TypeDef* W_Port, uint16_t W_Pin,
								IRQn_Type IRQn,
								uint32_t PreemptPriorty,
								uint32_t SubPriority)
{
	PortPinPair_t N = {N_Port, N_Pin},
	S = {S_Port, S_Pin},
	E = {E_Port, E_Pin},
	W = {W_Port, W_Pin};
	eightDStick_Init_EXTI(stick, N, S, E, W, IRQn, PreemptPriorty, SubPriority);
}

eightDStick_Status_t eightDStick_IsIdle(eightDStick_t* stick) {
	assert(stick);
	return (eightDStick_GetPortState(stick) == 0 ?  eightDStick_Idle : eightDStick_Active);
}

void eightDStick_WaitWhileIdle(eightDStick_t* stick) {
	assert(stick);
	while (eightDStick_GetPortState(stick) == 0);
}

void eightDStick_WaitWhileBusy(eightDStick_t* stick) {
	assert(stick);
	while (eightDStick_GetPortState(stick) != 0);
}

eightDStick_Direction_t eightDStick_GetDirection(eightDStick_t* stick) {
	assert(stick);
	while (eightDStick_GetPortState(stick) == 0);									// polling
	HAL_Delay(eightDStick_DebounceWait);											// ritardo nella lettura
	eightDStick_Direction_t direction = eightDStick_GetDirection_NoWait(stick);		// lettura della direzione
	while (eightDStick_GetPortState(stick) != 0);									// polling
	HAL_Delay(eightDStick_DebounceWait);											// ritardo debouncing
	return direction;
}

eightDStick_Direction_t eightDStick_GetDirection_NoWait(eightDStick_t* stick) {
	assert(stick);
	uint8_t status = 0;
	status = eightDStick_GetPortState(stick);
	// bit7 : N
	// bit6 : S
	// bit5 : E
	// bit4 : W

	switch (status) {
		case 0x80: return North;
		case 0x40: return South;
		case 0x20: return East;
		case 0x10: return West;
		case 0xA0: return NorthEast;
		case 0x90: return NorthWest;
		case 0x60: return SouthEast;
		case 0x50: return SouthWest;
		default: return NoDirection;
	}
}

void eightDStick_ClearEXTI(eightDStick_t* stick) {
	__HAL_GPIO_EXTI_CLEAR_IT(stick->N.Pin);
	__HAL_GPIO_EXTI_CLEAR_IT(stick->S.Pin);
	__HAL_GPIO_EXTI_CLEAR_IT(stick->W.Pin);
	__HAL_GPIO_EXTI_CLEAR_IT(stick->E.Pin);
	__HAL_GPIO_EXTI_CLEAR_FLAG(stick->N.Pin);
	__HAL_GPIO_EXTI_CLEAR_FLAG(stick->S.Pin);
	__HAL_GPIO_EXTI_CLEAR_FLAG(stick->W.Pin);
	__HAL_GPIO_EXTI_CLEAR_FLAG(stick->E.Pin);
	HAL_NVIC_ClearPendingIRQ(stick->IRQn);
}

int eightDStick_ValidatePair(eightDStick_t* stick) {
	const int array_dim = 4;
	int i, j;
	const PortPinPair_t pair[] = {stick->N, stick->S, stick->E, stick->W};
	for (i = 0; i < array_dim; i++)
		for (j = i+1; j < array_dim; j++)
			if (	pair[i].Port != 0 &&
					pair[j].Port != 0 &&
					pair[i].Port == pair[j].Port &&
					pair[i].Pin == pair[j].Pin)
				return 0;
	return 1;
}

int eightDStick_ValidatePair_EXTI(eightDStick_t* stick)
{
	const int array_dim = 4;
	int i, j;
	const PortPinPair_t pair[] = {stick->N, stick->S, stick->E, stick->W};

	// test di disgiunzione delle coppie
	for (i = 0; i < array_dim; i++)
		for (j = i+1; j < array_dim; j++)
			if (	pair[i].Port != 0 &&
					pair[j].Port != 0 &&
					pair[i].Port == pair[j].Port &&
					pair[i].Pin == pair[j].Pin)
				return 0;

	// test di compatibilita' con la linea EXTI 9_5
	if(stick->IRQn == EXTI_LINE_9_5)
		for (i = 0; i < array_dim; i++)
			if (
					pair[i].Pin != GPIO_PIN_5 &&
					pair[i].Pin != GPIO_PIN_6 &&
					pair[i].Pin != GPIO_PIN_7 &&
					pair[i].Pin != GPIO_PIN_8 &&
					pair[i].Pin != GPIO_PIN_9
			)
				return 0;

	// test di compatibilita' con la linea EXTI 15_10
	if (stick->IRQn == EXTI_LINE_15_10)
		for (i = 0; i < array_dim; i++)
			if (
					pair[i].Pin != GPIO_PIN_10 &&
					pair[i].Pin != GPIO_PIN_11 &&
					pair[i].Pin != GPIO_PIN_12 &&
					pair[i].Pin != GPIO_PIN_13 &&
					pair[i].Pin != GPIO_PIN_14 &&
					pair[i].Pin != GPIO_PIN_15
			)
				return 0;
	return 1;
}

uint8_t	eightDStick_GetPortState(eightDStick_t* stick)
{
	uint8_t status = 0;
	// bit7 : N
	// bit6 : S
	// bit5 : E
	// bit4 : W
	status |= (HAL_GPIO_ReadPin(stick->N.Port, stick->N.Pin) == GPIO_PIN_SET ? 0x80 : 0x00);
	status |= (HAL_GPIO_ReadPin(stick->S.Port, stick->S.Pin) == GPIO_PIN_SET ? 0x40 : 0x00);
	status |= (HAL_GPIO_ReadPin(stick->E.Port, stick->E.Pin) == GPIO_PIN_SET ? 0x20 : 0x00);
	status |= (HAL_GPIO_ReadPin(stick->W.Port, stick->W.Pin) == GPIO_PIN_SET ? 0x10 : 0x00);
	return status;
}

void eightDStick_ConfigurePin(eightDStick_t* stick)
{
	const int array_dim = 4;
	int i;
	const PortPinPair_t pair[] = {stick->N, stick->S, stick->W, stick->S};
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	for (i = 0; i < array_dim; i++) {
		GPIO_InitStruct.Pin = pair[i].Pin;
		HAL_GPIO_Init(pair[i].Port, &GPIO_InitStruct);
	}
}

void eightDStick_ConfigurePin_EXTI(	eightDStick_t* stick)
{
	const int array_dim = 4;
	int i;
	const PortPinPair_t pair[] = {stick->N, stick->S, stick->W, stick->S};
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;	// pin configurati per generare interrupt su rising-edge
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	for (i = 0; i < array_dim; i++) {
		GPIO_InitStruct.Pin = pair[i].Pin;
		HAL_GPIO_Init(pair[i].Port, &GPIO_InitStruct);
	}
}
