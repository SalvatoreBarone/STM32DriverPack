/**
 * @file hexKeyboard.h
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of STM32DriverPack
 *
 * STM32DriverPack is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * STM32DriverPack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

/* Changelog
 * 17 agosto 2016
 * 	Completata l'implementazione delle funzioni, la documentazione delle stesse ed il testing.
 *
 * 18 agosto 2016
 *   Modifiche estese volte a introdurre il supporto agli interrupt.
 *   	- Rettifica della documentazione delle funzioni di inizializzazione
 *   	- Aggiunta di esempi d'uso delle funzioni di inizializzazione
 *   	- Aggiunto esempio d'uso in ISR
 *   	- Alla struttura HexKeypad_t sono stati aggiunti i campi relativi alla linea interrupr,
 *   	  alla priorita' e sub-priorita'
 *   	- Aggiunte le funzioni dedicate all'inizializzazione della struttura HexKeypad_t che
 *   	  permettano l'uso degli interrupt
 *   	- Aggiunte e implementate tutte le funzioni private per il supporto agli interrupt, quali
 *   	  quelle di verifica per l'inizializzazione e configurazione dei GPIO associati ad uno
 *   	  stick
 *   	- Test eseguito sulle funzioni aggiunte al modulo
 *   	- Correzione di un bug che causava il blocco del programma quando viene usata la funzione
 *   	  HexKeypad_GetChar_NoPolling
 * 20 agosto 2016
 *   Introduzione del tipo HexKeypad_Status_t utilizzato per la lettura dello stato del device
 *
 * 17-19 maggio 2017
 *  - Eliminazione del tipo HD44780_error_t
 *  - uso di assert per la verifica dei parametri passati alle funzioni
 *  - miglioramento del supporto agli interrupt
 *  - completamento della documentazione doxygen
 *  - test di modulo
 */

/**
 * @addtogroup InputDevice
 * @{
 *
 * @defgroup HexKeyboard
 * @{
 *
 * @brief Driver per keypad a 16 tasti
 * 
 * Questo modulo permette di utilizzare un semplice tastierino esadecimale come device di input.
 * Il driver e' concepito per gestire piu' keypad, ciascuno astratto da un oggetto HexKeypad_t,
 * anche contemporaneamente.<br>
 * Il keypad gestito e' di tipo "a matrice", come quello riportato nello schema di seguito. 
 * 
 * @image html hex-keypad.png
 * @image latex hex-keypad.png
 * 
 * Il driver configura i GPIO per l'interfacciamento utilizzando i resistori di pull-down
 * automaticamente, in modo da limitare l'hardware aggiuntivo necessario.<br>
 * La struttura HexKeypad_t specifica quali siano i pin del microcontrollore che pilotano un
 * determinato segnale del device. Ciascuno dei pin, cosi' come previsto dalla libreria HAL,
 * e' identificato attraverso una coppia porta-pin.<br>
 * L'assegnazione segnale-coppia, quindi l' inizializzazione della struttura relativa ad un device
 * DEVE essere effettuatatassativamente utilizzando le funzioni di inizializzazione fornite dal
 * driver, le quali provvedono anche ad effettuare un test di connessione volto ad individuare
 * eventuali segnali erroneamente associati.<br>
 * Per i dettagli si rimanda alla documentazione delle specifiche funzioni ed alla documentazione
 * esterna che accompagna il modulo, reperibile nella cartella Doc.
 */

#ifndef __HEX_KEYPAD_H__
#define	__HEX_KEYPAD_H__

#include <inttypes.h>
#include "common.h"

/**
 * @brief Tempo di debouncing
 * 
 * Il valore di default è 50, determinato empiricamente. Puo' essere modificato a piacimento
 * cambiando il valore alla macro seguente.
 */
#define HexKeypad_DebounceWait 50

/**
 * @brief Struttura usata per la gestione di un keypad.
 * 
 * Il keypad gestito e' di tipo "a matrice", come quello riportato nello schema di seguito. 
 * 
 * @image html hex-keypad.png
 * @image latex hex-keypad.png
 * 
 * Ciascuno dei terminali per le colonne e ciascuno dei terminali per le righe vanno connessi
 * ad un GPIO.
 */
typedef struct {
	PortPinPair_t ColumnA;		/**< Coppia porta-pin connessa al terminale ColumnA */
	PortPinPair_t ColumnB;		/**< Coppia porta-pin connessa al terminale ColumnB */
	PortPinPair_t ColumnC;		/**< Coppia porta-pin connessa al terminale ColumnC */
	PortPinPair_t ColumnD;		/**< Coppia porta-pin connessa al terminale ColumnD */
	PortPinPair_t Row1;			/**< Coppia porta-pin connessa al terminale Row1 */
	PortPinPair_t Row2;			/**< Coppia porta-pin connessa al terminale Row1 */
	PortPinPair_t Row3;			/**< Coppia porta-pin connessa al terminale Row1 */
	PortPinPair_t Row4;			/**< Coppia porta-pin connessa al terminale Row1 */
	IRQn_Type IRQn;				/**< 	linea EXTI a cui lo stick fa riferimento.
     									Per la lista completa delle linee disponibili per un certo device si faccia riferimento
            							al file Driver/CMSIS/ST/STM32Fxxx/Include/stm32fxxxxxxx.h
                 						Ad esempio, quello per il processore STM32F303VCT6 usato per il testing e' il file
                     					Driver/CMSIS/ST/STM32F3xx/Include/stm32f3030xc.h */
	uint32_t PreemptPriorty;	/**<	priorita' di pre-emption per la linea IRQn. Deve essere un valore compreso tra 0 e 15.
									 	Valori bassi indicano priorita' alta. */
	uint32_t SubPriority;		/**<	sub-priorita' per la linea IRQn. Deve essere un valore compreso tra 0 e 15.
     									Valori bassi indicano priorita' alta. */
} HexKeypad_t;

/**
 * @brief Mappa dei tasti associati alle coppie riga-colonna del keypad
 * 
 * La mappa dei tasti rispecchia lo schema riportato di seguito.
 * 
 * @image html hex-keypad.png
 * @image latex hex-keypad.png
 */
typedef enum {
//  A       B	 	C	 	D
	A1, 	B1, 	C1, 	D1,	// riga 1
	A2, 	B2, 	C2, 	D2,	// riga 2
	A3, 	B3,		C3, 	D3,	// riga 3
	A4, 	B4, 	C4, 	D4,	// riga 4
	NoKey
} HexKeypad_Key_t;

/**
 * @brief Inizializza il devide in modo da consentire la lettura di un tasto di un keypad
 *
 * Verifica che le coppie porta-pin per l'interfacciamento siano corrette e disgiunte, configura
 * i pin GPIO per l'interfacciamento con il device. Non utilizza interrupt.
 *
 * @warning Questa funzione di inizializzazione non consente l'utilizzo degli interrupt esterni
 * sul cambiamento dello stato dei pin.
 *
 * @warning Non modificare i campi della struttura eightDStick_t dopo che essa sia stata
 * inizializzata.
 *
 * @param keypad puntatore all'oggetto HexKeypad_t da inizializzare
 * @param ColumnA coppia porta-pin associata alla colonna A del keypad
 * @param ColumnB coppia porta-pin associata alla colonna B del keypad
 * @param ColumnC coppia porta-pin associata alla colonna C del keypad
 * @param ColumnD coppia porta-pin associata alla colonna D del keypad
 * @param Row1 coppia porta-pin associata alla riga 1 del keypad
 * @param Row2 coppia porta-pin associata alla riga 2 del keypad
 * @param Row3 coppia porta-pin associata alla riga 3 del keypad
 * @param Row4 coppia porta-pin associata alla riga 4 del keypad
 *
 * @code
 * PortPinPair_t	ColumnA = {ColumnA_Port, ColumnA_Pin},
 *					ColumnB = {ColumnB_Port, ColumnB_Pin},
 *					ColumnC = {ColumnC_Port, ColumnC_Pin},
 *					ColumnD = {ColumnD_Port, ColumnD_Pin},
 *					Row1 = {Row1_Port, Row1_Pin},
 *					Row2 = {Row2_Port, Row2_Pin},
 *					Row3 = {Row3_Port, Row3_Pin},
 *					Row4 = {Row4_Port, Row4_Pin};
 * HexKeypad_t keypad;
 * HexKeypad_Init(keypad, ColumnA, ColumnB, ColumnC, ColumnD, Row1, Row2, Row3, Row4);
 * @endcode
 */
void HexKeypad_Init(	HexKeypad_t* keypad,
						PortPinPair_t ColumnA,
						PortPinPair_t ColumnB,
						PortPinPair_t ColumnC,
						PortPinPair_t ColumnD,
						PortPinPair_t Row1,
						PortPinPair_t Row2,
						PortPinPair_t Row3,
						PortPinPair_t Row4);

/**
 * @brief Inizializza il devide in modo da consentire la lettura di un tasto di un keypad
 *
 * Verifica che le coppie porta-pin per l'interfacciamento siano corrette e disgiunte, configura
 * i pin GPIO per l'interfacciamento con il device. Imposta i GPIO a cui il keypad e' connesso
 * affinche' generino interrupt quando viene premuto un tasto.
 *
 * @warning Non modificare i campi della struttura eightDStick_t dopo che essa sia stata
 * inizializzata.
 *
 * @param keypad puntatore all'oggetto HexKeypad_t da inizializzare
 * @param ColumnA coppia porta-pin associata alla colonna A del keypad
 * @param ColumnB coppia porta-pin associata alla colonna B del keypad
 * @param ColumnC coppia porta-pin associata alla colonna C del keypad
 * @param ColumnD coppia porta-pin associata alla colonna D del keypad
 * @param Row1 coppia porta-pin associata alla riga 1 del keypad
 * @param Row2 coppia porta-pin associata alla riga 2 del keypad
 * @param Row3 coppia porta-pin associata alla riga 3 del keypad
 * @param Row4 coppia porta-pin associata alla riga 4 del keypad
 * @param IRQn linea EXTI a cui lo stick fa riferimento.
 * Per la lista completa delle linee disponibili per un certo device si faccia riferimento al file
 * Driver/CMSIS/ST/STM32Fxxx/Include/stm32fxxxxxxx.h
 * Ad esempio, quello per il processore STM32F303VCT6 usato per il testing e' il file
 * Driver/CMSIS/ST/STM32F3xx/Include/stm32f3030xc.h
 * @param PreemptPriorty priorita' di pre-emption per la linea IRQn. Deve essere un valore
 * compreso tra 0 e 15. Valori bassi indicano priorita' alta.
 * @param SubPriority sub-priorita' per la linea IRQn. Deve essere un valore compreso tra 0 e 15.
 * Valori bassi indicano priorita' alta.
 *
 * @code
 * PortPinPair_t ColumnA = {ColumnA_Port, ColumnA_Pin},
 * ColumnB = {ColumnB_Port, ColumnB_Pin},
 * ColumnC = {ColumnC_Port, ColumnC_Pin},
 * ColumnD = {ColumnD_Port, ColumnD_Pin},
 * Row1 = {Row1_Port, Row1_Pin},
 * Row2 = {Row2_Port, Row2_Pin},
 * Row3 = {Row3_Port, Row3_Pin},
 * Row4 = {Row4_Port, Row4_Pin};
 * HexKeypad_Init_EXTI(	keypad,
 * 						ColumnA, ColumnB, ColumnC, ColumnD,
 * 						Row1, Row2, Row3, Row4,
 * 						IRQn, PreemptPriorty, SubPriority);
 * @endcode
 */
void HexKeypad_Init_EXTI(	HexKeypad_t* keypad,
							PortPinPair_t ColumnA,
							PortPinPair_t ColumnB,
							PortPinPair_t ColumnC,
							PortPinPair_t ColumnD,
							PortPinPair_t Row1,
							PortPinPair_t Row2,
							PortPinPair_t Row3,
							PortPinPair_t Row4,
							IRQn_Type IRQn,
							uint32_t PreemptPriorty,
							uint32_t SubPriority);

/**
 * @brief Inizializza il devide in modo da consentire la lettura di un tasto di un keypad
 *
 * Verifica che le coppie porta-pin per l'interfacciamento siano corrette e disgiunte, configura
 * i pin GPIO per l'interfacciamento con il device. Non utilizza interrupt.
 *
 * @warning Questa funzione di inizializzazione non consente l'utilizzo degli interrupt esterni
 * sul cambiamento dello stato dei pin.
 *
 * @warning Non modificare i campi della struttura eightDStick_t dopo che essa sia stata
 * inizializzata.
 *
 * @param keypad puntatore all'oggetto HexKeypad_t da inizializzare
 * @param ColumnA_Port porta associata alla colonna A del keypad
 * @param ColumnA_Pin pin di ColumnA_Port associato alla colonna A del keypad
 * @param ColumnB_Port porta associata alla colonna B del keypad
 * @param ColumnB_Pin pin di ColumnB_Port associato alla colonna B del keypad
 * @param ColumnC_Port porta associata alla colonna C del keypad
 * @param ColumnC_Pin pin di ColumnC_Port associato alla colonna C del keypad
 * @param ColumnD_Port porta associata alla colonna D del keypad
 * @param ColumnD_Pin pin di ColumnD_Port associato alla colonna D del keypad
 * @param Row1_Port	porta associata alla riga 1 del keypad
 * @param Row1_Pin pin di Row1_Port associato alla riga 1 del keypad
 * @param Row2_Port	porta associata alla riga 2 del keypad
 * @param Row2_Pin pin di Row2_Port associato alla riga 2 del keypad
 * @param Row3_Port	porta associata alla riga 3 del keypad
 * @param Row3_Pin pin di Row3_Port associato alla riga 3 del keypad
 * @param Row4_Port	porta associata alla riga 4 del keypad
 * @param Row4_Pin pin di Row4_Port associato alla riga 4 del keypad
 *
 * @code
 * HexKeypad_t keypad;
 * HexKeypad_Init_v2(	&keypad,
 *						GPIOD, GPIO_PIN_12,		// column A
 *						GPIOD, GPIO_PIN_13,		// column B
 *						GPIOD, GPIO_PIN_14,		// column C
 *						GPIOD, GPIO_PIN_15,		// column D
 *						GPIOD, GPIO_PIN_11,		// row 1
 *						GPIOD, GPIO_PIN_10,		// row 2
 *						GPIOD, GPIO_PIN_9,		// row 3
 *						GPIOD, GPIO_PIN_8);		// row 4
 * @endcode
 */
void HexKeypad_Init_v2(	HexKeypad_t* keypad,
						GPIO_TypeDef* ColumnA_Port, uint16_t ColumnA_Pin,
						GPIO_TypeDef* ColumnB_Port, uint16_t ColumnB_Pin,
						GPIO_TypeDef* ColumnC_Port, uint16_t ColumnC_Pin,
						GPIO_TypeDef* ColumnD_Port, uint16_t ColumnD_Pin,
						GPIO_TypeDef* Row1_Port, uint16_t Row1_Pin,
						GPIO_TypeDef* Row2_Port, uint16_t Row2_Pin,
						GPIO_TypeDef* Row3_Port, uint16_t Row3_Pin,
						GPIO_TypeDef* Row4_Port, uint16_t Row4_Pin);

/**
 * @brief Inizializza il devide in modo da consentire la lettura di un tasto di un keypad
 *
 * Verifica che le coppie porta-pin per l'interfacciamento siano corrette e disgiunte, configura
 * i pin GPIO per l'interfacciamento con il device. Imposta i GPIO a cui il keypad e' connesso
 * affinche' generino interrupt quando viene premuto un tasto.
 *
 * @warning Non modificare i campi della struttura eightDStick_t dopo che essa sia stata
 * inizializzata.
 *
 * @param keypad puntatore all'oggetto HexKeypad_t da inizializzare
 * @param ColumnA_Port porta associata alla colonna A del keypad
 * @param ColumnA_Pin pin di ColumnA_Port associato alla colonna A del keypad
 * @param ColumnB_Port porta associata alla colonna B del keypad
 * @param ColumnB_Pin pin di ColumnB_Port associato alla colonna B del keypad
 * @param ColumnC_Port porta associata alla colonna C del keypad
 * @param ColumnC_Pin pin di ColumnC_Port associato alla colonna C del keypad
 * @param ColumnD_Port porta associata alla colonna D del keypad
 * @param ColumnD_Pin pin di ColumnD_Port associato alla colonna D del keypad
 * @param Row1_Port	porta associata alla riga 1 del keypad
 * @param Row1_Pin pin di Row1_Port associato alla riga 1 del keypad
 * @param Row2_Port	porta associata alla riga 2 del keypad
 * @param Row2_Pin pin di Row2_Port associato alla riga 2 del keypad
 * @param Row3_Port	porta associata alla riga 3 del keypad
 * @param Row3_Pin pin di Row3_Port associato alla riga 3 del keypad
 * @param Row4_Port	porta associata alla riga 4 del keypad
 * @param Row4_Pin pin di Row4_Port associato alla riga 4 del keypad
 * @param IRQn linea EXTI a cui lo stick fa riferimento.
 * Per la lista completa delle linee disponibili per un certo device si faccia riferimento al file
 * Driver/CMSIS/ST/STM32Fxxx/Include/stm32fxxxxxxx.h
 * Ad esempio, quello per il processore STM32F303VCT6 usato per il testing e' il file
 * Driver/CMSIS/ST/STM32F3xx/Include/stm32f3030xc.h
 * @param PreemptPriorty priorita' di pre-emption per la linea IRQn. Deve essere un valore
 * compreso tra 0 e 15. Valori bassi indicano priorita' alta.
 * @param SubPriority sub-priorita' per la linea IRQn. Deve essere un valore compreso tra 0 e 15.
 * Valori bassi indicano priorita' alta.
 *
 * @code
 * HexKeypad_Init_EXTI_v2(	&keypad,
 * 							GPIOD, GPIO_PIN_12,		// column A
 * 							GPIOD, GPIO_PIN_13,		// column B
 * 							GPIOD, GPIO_PIN_14,		// column C
 * 							GPIOD, GPIO_PIN_15,		// column D
 * 							GPIOD, GPIO_PIN_11,		// row 1
 * 							GPIOD, GPIO_PIN_10,		// row 2
 * 							GPIOD, GPIO_PIN_9,		// row 3
 * 							GPIOD, GPIO_PIN_8,		// row 4
 * 							EXTI15_10_IRQn,
 * 							15,
 * 							15);
 * @endcode
 */
void HexKeypad_Init_EXTI_v2(	HexKeypad_t* keypad,
								GPIO_TypeDef* ColumnA_Port, uint16_t ColumnA_Pin,
								GPIO_TypeDef* ColumnB_Port, uint16_t ColumnB_Pin,
								GPIO_TypeDef* ColumnC_Port, uint16_t ColumnC_Pin,
								GPIO_TypeDef* ColumnD_Port, uint16_t ColumnD_Pin,
								GPIO_TypeDef* Row1_Port, uint16_t Row1_Pin,
								GPIO_TypeDef* Row2_Port, uint16_t Row2_Pin,
								GPIO_TypeDef* Row3_Port, uint16_t Row3_Pin,
								GPIO_TypeDef* Row4_Port, uint16_t Row4_Pin,
								IRQn_Type IRQn,
								uint32_t PreemptPriorty,
								uint32_t SubPriority);

/**
 * @brief Stato del Keypad
 */
typedef enum {
	HexKeypad_Idle,	//!< Il Keypad e' inattivo
	HexKeypad_Active //!< Il Keypad e' attivo, uno dei tasti e' stato premuto
} HexKeypad_Status_t;

/**
 * @brief Consente di verificare se un keypad sia attivo o meno.
 *
 * @param keypad puntatore all'oggetto HexKeypad_t di cui rilevare la pressione di un tasto;
 * @retval HexKeypad_Idle se il keypad e' inattivo
 * @retval HexKeypad_Active se il keypad e' attivo, cioe' se uno dei tasti e' stato premuto
 */
HexKeypad_Status_t HexKeypad_IsIdle(HexKeypad_t* keypad);

/**
 * @brief Blocca l'esecuzione del programma fino a quando il keypad e' inattivo.
 *
 * L'esecuzione riprende non appena il keypad smette di essere inattivo, vale a dire non appena
 * viene premuto un tasto.
 *
 * @param keypad puntatore all'oggetto HexKeypad_t
 *
 * @code
 * 	// Esempio di Interrupt-Handler
 * 	void EXTI15_10_IRQHandler(void)
 *	{
 *		HexKeypad_WaitWhileIdle(&keypad);
 * 		key = HexKeypad_GetChar_NoWait(&keypad);
 * 		HexKeypad_WaitWhileBusy(&keypad);
 * 		HexKeypad_ClearEXTI(&keypad);
 *	}
 * @endcode
 */
void HexKeypad_WaitWhileIdle(HexKeypad_t* keypad);

/**
 * @brief Blocca l'esecuzione del programma fino a quando il tastierino e' attivo.
 *
 * L'esecuzione del programma riprende non appena il keypad viene liberato, cioe' quanto nessuno
 * dei tasti e' premuto.
 *
 * @param keypad puntatore all'oggetto HexKeypad_t
 *
 * @code
 * 	// Esempio di Interrupt-Handler
 * 	void EXTI15_10_IRQHandler(void)
 *	{
 *		HexKeypad_WaitWhileIdle(&keypad);
 * 		key = HexKeypad_GetChar_NoWait(&keypad);
 * 		HexKeypad_WaitWhileBusy(&keypad);
 * 		HexKeypad_ClearEXTI(&keypad);
 *	}
 * @endcode
 */
void HexKeypad_WaitWhileBusy(HexKeypad_t* keypad);

/**
 * @brief Restituisce l'identificativo del tasto premuto.
 *
 * La funzione blocca l'esecuzione del programma attendendo la pressione di un tasto.
 *
 * @param keypad puntatore all'oggetto HexKeypad_t di cui rilevare la pressione di un tasto;
 * @return identificativo del tasto premuto
 *
 * @warning Restituisce NoKey nel caso in cui non sia stato possibile stabilire quale tasto sia stato premuto
 *
 * @code
 * char keymap[] = {
 * 			'1', '2', '3', '+',
 * 			'4', '5', '6', '-',
 * 			'7', '8', '9', '*',
 * 			'C', '.', '=', '/',
 * 			'?'
 * };
 * HexKeypad_Key_t key = HexKeypad_GetChar(&keypad);
 * switch (key)
 * {
 * case A4:
 * 	HD44780_Clear(lcd);
 * 	break;
 * default:
 * 	HD44780_Printc(lcd, keymap[(int)key]);
 * 	break;
 * }
 * @endcode
 */

HexKeypad_Key_t HexKeypad_GetChar(HexKeypad_t* keypad);

/**
 * @brief Restituisce l'identificativo del tasto premuto, se ne e' stato premuto uno.
 *
 * La funzione non blocca l'esecuzione del programma. Puo' essere utilizzata all'interno della
 * ISR che viene attivata al variare di un blocco di GPIO.
 *
 * @param keypad puntatore all'oggetto HexKeypad_t di cui rilevare la pressione di un tasto;
 * @return identificativo del tasto premuto;
 *
 * @warning Restituisce NoKey nel caso in cui non sia stato possibile stabilire quale tasto sia stato premuto
 *
 * @code
 * 	// Esempio di Interrupt-Handler
 * 	void EXTI15_10_IRQHandler(void)
 *	{
 *		HexKeypad_WaitWhileIdle(&keypad);
 * 		key = HexKeypad_GetChar_NoWait(&keypad);
 * 		HexKeypad_WaitWhileBusy(&keypad);
 * 		HexKeypad_ClearEXTI(&keypad);
 *	}
 * @endcode
 */
HexKeypad_Key_t HexKeypad_GetChar_NoWait(HexKeypad_t* keypad);

/**
 * @brief Pulisce l'infrastruttura per la gestione delle interrupt dopo averne servita una
 *
 * La funzione permette di resettare i flag usati da un keypad durante gestione delle interrupt.
 *
 * @param keypad puntatore all'oggetto HexKeypad_t di cui rilevare la pressione di un tasto;
 *
 * @code
 * 	// Esempio di Interrupt-Handler
 * 	void EXTI15_10_IRQHandler(void)
 *	{
 *		HexKeypad_WaitWhileIdle(&keypad);
 * 		key = HexKeypad_GetChar_NoWait(&keypad);
 * 		HexKeypad_WaitWhileBusy(&keypad);
 * 		HexKeypad_ClearEXTI(&keypad);
 *	}
 * @endcode
 */
void HexKeypad_ClearEXTI(HexKeypad_t* keypad);


#endif

/**
 * @}
 * @}
 */
