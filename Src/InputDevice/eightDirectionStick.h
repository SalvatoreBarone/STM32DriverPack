/**
 * @file eightDirectionStick.h
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of STM32DriverPack
 *
 * STM32DriverPack is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * STM32DriverPack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */


/* Changelog
 * 17 agosto 2016
 * 	Completata l'implementazione delle funzioni, la documentazione delle stesse ed il testing.
 *
 * 18 agosto 2016
 *   Modifiche estese volte a introdurre il supporto agli interrupt.
 *   	- Rettifica della documentazione delle funzioni di inizializzazione
 *   	- Aggiunta di esempi d'uso delle funzioni di inizializzazione
 *   	- Aggiunto esempio d'uso in ISR
 *   	- Alla struttura eightDStick_t sono stati aggiunti i campi relativi alla linea interrupr,
 *   	  alla priorita' e sub-priorita'
 *   	- Aggiunte le funzioni dedicate all'inizializzazione della struttura eightDStick_t che
 *   	  permettano l'uso degli interrupt
 *   	- Aggiunte e implementate tutte le funzioni private per il supporto agli interrupt, quali
 *   	  quelle di verifica per l'inizializzazione e configurazione dei GPIO associati ad uno
 *   	  stick
 *   	- Test eseguito sulle funzioni aggiunte al modulo
 *
 * 20 agosto 2016
 *   Introduzione del tipo eightDStick_Status_t utilizzato per la lettura dello stato del device
 *
 * 17-19 maggio 2017
 *  - Eliminazione del tipo eightDStick_error_t
 *  - uso di assert per la verifica dei parametri passati alle funzioni
 *  - miglioramento del supporto agli interrupt
 *  - completamento della documentazione doxygen
 *  - test di modulo
 */

/**
 * @addtogroup InputDevice
 * @{
 *
 * @defgroup eightDStick
 * @{
 * 
 * @brief Driver per stick a due assi.
 *
 * Questo modulo permette di utilizzare un semplice joystick come device di input.
 * Il modulo e' pensato per gestire un joystick a 2 assi con otto direzioni. Possono essere
 * gestiti piu' stick usando oggetti diversi.<br>
 * 
 * @image html joystick.png
 * @image latex joystick.png
 * 
 * Il driver configura i GPIO per l'interfacciamento utilizzando i resistori di pull-down automaticamente,
 * in modo da limitare l'hardware aggiuntivo necessario.<br>
 * La struttura eightDStick_t specifica quali siano i pin del microcontrollore che pilotano un
 * determinato segnale del device. Ciascuno dei pin, cosi' come previsto dalla libreria HAL,
 * e' identificato attraverso una coppia porta-pin.<br>
 * L'assegnazione segnale-coppia, quindi l' inizializzazione della struttura relativa ad un device
 * DEVE essere effettuatatassativamente utilizzando le funzioni di inizializzazione fornite dal
 * driver, le quali provvedono anche ad effettuare un test di connessione volto ad individuare
 * eventuali segnali erroneamente associati.<br>
 * Le funzioni di inizializzazione messe a disposizione dal modulo sono:
 * - eightDStick_Init() e eightDStick_Init_v2() che inizializzano il device in modo da consentire la lettura dello stato di uno
 * 	 stick;
 * - eightDStick_Init_EXTI() e eightDStick_Init_EXTI_v2() che configurano il sottosistema delle interruzioni ed inizializzano il
 *   device in modo da consentire la lettura dello stato di uno stick.<br>
 *
 * Per interagire con lo stick vengono fornite le seguenti funzioni:
 * - eightDStick_IsIdle() che restituisce lo stato attivo/non-attivo di uno stick;
 * - eightDStick_WaitWhileIdle() che consente di porre il programma in attesa attiva fin quando lo stick e' inattivo;
 * - eightDStick_WaitWhileBusy() che consente di porre il programma in attesa attiva fin quando lo stick e' puntato in una
 *   direzione particolare;
 * - eightDStick_GetDirection() che consente di attendere che lo stick venga puntato in una direzione, ottenendola;
 * - eightDStick_GetDirection_NoWait() che consente di ottenere l'eventuale direzione in cui punta uno stick, senza attendere
 *   che esso venga puntato effettivamente, il che la rende adatta all'implementazione di una ISR;
 * - eightDStick_ClearEXTI() che consente di pulire l'infrastruttura per la gestione delle interrupt dopo aver servito
 *   l'interruzione sollevata dallo stick.
 */

#ifndef __EIGTH_DIRECTION_STICK_H
#define __EIGTH_DIRECTION_STICK_H

#include <inttypes.h>

#include "common.h"

/**
 * @brief Tempo di debouncing
 * 
 * Il valore di default è 50, determinato empiricamente. Puo' essere modificato a piacimento
 * cambiando il valore alla macro seguente.
 */
#define eightDStick_DebounceWait	50

/**
 * @brief L'oggetto di tipo eightDStick_t rappresenta un device joystick.
 *
 * Si veda la documentazione a corredo per i dettagli circuitali e di montaggio.
 *
 * La struttura raggruppa tutte le coppie porta-pin utilizzate per l'interfacciamento con il
 * device.
 * La struttura va inizializzata esclusivamente attraverso l'uso delle funzioni di inizializzazione.
 * E' fortemente sconsigliato inizializzare le coppie campo per campo manualmente in quanto, oltre
 * la mera inizializzazione, le suddette funzioni effettuano anche la validazione delle connessioni,
 * segnalando eventuali errori di configurazione.
 *
 * @warning Se i pin associati ai segnali di pilotaggio del device non sono correttamente
 * configurati come pin di output, il dispositivo non funzionera' correttamente.
 *
 * @warning Non modificare i campi della struttura eightDStick_t dopo che essa sia stata
 * inizializzata.
 */
typedef struct {
	PortPinPair_t N;			/**< coppia porta-pin associata al segnale "Nord" */
	PortPinPair_t S;			/**< coppia porta-pin associata al segnale "Sud" */
	PortPinPair_t E;			/**< coppia porta-pin associata al segnale "Est" */
	PortPinPair_t W;			/**< coppia porta-pin associata al segnale "Ovest" */
	IRQn_Type IRQn;				/**< 	linea EXTI a cui lo stick fa riferimento.
     									Per la lista completa delle linee disponibili per un certo device si faccia riferimento
            							al file Driver/CMSIS/ST/STM32Fxxx/Include/stm32fxxxxxxx.h
                 						Ad esempio, quello per il processore STM32F303VCT6 usato per il testing e' il file
                     					Driver/CMSIS/ST/STM32F3xx/Include/stm32f3030xc.h */
	uint32_t PreemptPriorty;	/**<	priorita' di pre-emption per la linea IRQn. Deve essere un valore compreso tra 0 e 15.
									 	Valori bassi indicano priorita' alta. */
	uint32_t SubPriority;		/**<	sub-priorita' per la linea IRQn. Deve essere un valore compreso tra 0 e 15.
     									Valori bassi indicano priorita' alta. */
} eightDStick_t;

/**
 * @brief Direzioni in cui puo' essere orientato lo stick
 */
typedef enum {
	North, 			//!< N 	nord
	NorthWest,		//!< NW nord-ovest
	West, 			//!< W 	ovest
	SouthWest,		//!< SW sud-ovest
	South, 			//!< S	sud
	SouthEast,		//!< SE	sud-est
	East, 			//!< E	est
	NorthEast, 		//!< NE	nord-est
	NoDirection 	//!< No direction
} eightDStick_Direction_t;

/**
 * @brief Inizializza il device in modo da consentire la lettura dello stato di uno stick
 *
 * Verifica che le coppie porta-pin per l'interfacciamento siano corrette e disgiunte, configura
 * i pin GPIO per l'interfacciamento con il device. Non utilizza interrupt.
 *
 * @warning Questa funzione di inizializzazione non consente l'utilizzo degli interrupt esterni
 * sul cambiamento dello stato dei pin.
 *
 * @warning Non modificare i campi della struttura eightDStick_t dopo che essa sia stata
 * inizializzata.
 *
 * @param[inout]	stick	oggeto di tipo eightDStick_t da inizializzare
 * @param[in] 		N		coppia porta-pin associata al segnale "Nord"
 * @param[in]		S		coppia porta-pin associata al segnale "Sud"
 * @param[in]		E		coppia porta-pin associata al segnale "Est"
 * @param[in]		W		coppia porta-pin associata al segnale "Ovest"
 *
 * @code
 * PortPinPair_t 	N = {N_Port, N_Pin},
 *					S = {S_Port, S_Pin},
 *	    			E = {E_Port, E_Pin},
 *					W = {W_Port, W_Pin};
 * eightDStick_Init(&stick, N, S, E, W);
 * @endcode
 */
void eightDStick_Init(	eightDStick_t* stick,
						PortPinPair_t N,
						PortPinPair_t S,
						PortPinPair_t E,
						PortPinPair_t W);

/**
 * @brief Inizializza il device in modo da consentire la lettura dello stato di uno stick
 *
 * Verifica che le coppie porta-pin per l'interfacciamento siano corrette e disgiunte, configura
 * i pin GPIO per l'interfacciamento con il device. Predispone l'utilizzo degli interrupt,
 * configurando i pin affinche' venga generato un interrupt sul fronte di salita.
 *
 * @warning Non modificare i campi della struttura eightDStick_t dopo che essa sia stata
 * inizializzata.
 *
 * @param[inout]	stick	oggeto di tipo eightDStick_t da inizializzare
 * @param[in]		N		coppia porta-pin associata al segnale "Nord"
 * @param[in]		S		coppia porta-pin associata al segnale "Sud"
 * @param[in] 		E		coppia porta-pin associata al segnale "Est"
 * @param[in] 		W		coppia porta-pin associata al segnale "Ovest"
 * @param[in] 		IRQn	linea EXTI a cui lo stick fa riferimento.
 * 							Per la lista completa delle linee disponibili per un certo device si faccia riferimento al file
 *        					Driver/CMSIS/ST/STM32Fxxx/Include/stm32fxxxxxxx.h
 *             				Ad esempio, quello per il processore STM32F303VCT6 usato per il testing e' il file
 *                 			Driver/CMSIS/ST/STM32F3xx/Include/stm32f3030xc.h
 * @param[in] 		PreemptPriorty 	priorita' di pre-emption per la linea IRQn. Deve essere un valore
 * 									compreso tra 0 e 15. Valori bassi indicano priorita' alta.
 * @param[in] 		SubPriority 	sub-priorita' per la linea IRQn. Deve essere un valore compreso tra 0 e 15.
 * 									Valori bassi indicano priorita' alta.
 *
 * @code
 * PortPinPair_t 	N = {N_Port, N_Pin},
 *					S = {S_Port, S_Pin},
 *					E = {E_Port, E_Pin},
 *					W = {W_Port, W_Pin};
 * eightDStick_Init_EXTI(&stick, N, S, E, W, EXTI9_5_IRQn, 15, 0);
 * @endcode
 */
void eightDStick_Init_EXTI(	eightDStick_t* stick,
							PortPinPair_t N,
							PortPinPair_t S,
							PortPinPair_t E,
							PortPinPair_t W,
							IRQn_Type IRQn,
							uint32_t PreemptPriorty,
							uint32_t SubPriority);

/**
 * @brief Inizializza il device in modo da consentire la lettura dello stato di uno stick
 *
 * Verifica che le coppie porta-pin per l'interfacciamento siano corrette e disgiunte, configura
 * i pin GPIO per l'interfacciamento con il device. Non utilizza interrupt.
 *
 * @warning Questa funzione di inizializzazione non consente l'utilizzo degli interrupt esterni
 * sul cambiamento dello stato dei pin.
 *
 * @warning Non modificare i campi della struttura eightDStick_t dopo che essa sia stata
 * inizializzata.
 *
 * @param[inout]	stick	oggeto di tipo eightDStick_t da inizializzare
 * @param[in]		N_Port	porta associata al segnale "Nord"
 * @param[in]		N_Pin	pin di N_port associato al segnale "Nord"
 * @param[in]		S_Port	porta associata al segnale "Sud"
 * @param[in]		S_Pin	pin di S_port associato al segnale "Sud"
 * @param[in]		E_Port	porta associata al segnale "Est"
 * @param[in]		E_Pin	pin di E_port associato al segnale "Est"
 * @param[in]		W_Port	porta associata al segnale "Ovest"
 * @param[in]		W_Pin	pin di W_port associato al segnale "Ovest"
 *
 * @code
 * eightDStick_Error_t stick_error;
 * stick_error = eightDStick_Init_v2(	stick,
 * 										GPIOD, GPIO_PIN_12,
 *										GPIOD, GPIO_PIN_14,
 *										GPIOD, GPIO_PIN_15,
 *										GPIOD, GPIO_PIN_13);
 * @endcode
 */
void eightDStick_Init_v2(	eightDStick_t* stick,
							GPIO_TypeDef* N_Port, uint16_t N_Pin,
							GPIO_TypeDef* S_Port, uint16_t S_Pin,
							GPIO_TypeDef* E_Port, uint16_t E_Pin,
							GPIO_TypeDef* W_Port, uint16_t W_Pin);

/**
 * @brief Inizializza il device in modo da consentire la lettura dello stato di uno stick
 *
 * Verifica che le coppie porta-pin per l'interfacciamento siano corrette e disgiunte, configura
 * i pin GPIO per l'interfacciamento con il device. Predispone l'utilizzo degli interrupt,
 * configurando i pin affinche' venga generato un interrupt sul fronte di salita.
 *
 * @warning Non modificare i campi della struttura eightDStick_t dopo che essa sia stata
 * inizializzata.
 *
 * @param[inout]	stick	oggeto di tipo eightDStick_t da inizializzare
 * @param[in] 		N_Port	porta associata al segnale "Nord"
 * @param[in]		N_Pin	pin di N_port associato al segnale "Nord"
 * @param[in]		S_Port	porta associata al segnale "Sud"
 * @param[in]		S_Pin	pin di S_port associato al segnale "Sud"
 * @param[in]		E_Port	porta associata al segnale "Est"
 * @param[in]		E_Pin	pin di E_port associato al segnale "Est"
 * @param[in]		W_Port	porta associata al segnale "Ovest"
 * @param[in]		W_Pin	pin di W_port associato al segnale "Ovest"
 * @param[in]		IRQn 	linea EXTI a cui lo stick fa riferimento.
 * 							Per la lista completa delle linee disponibili per un certo device si faccia riferimento al file
 *        					Driver/CMSIS/ST/STM32Fxxx/Include/stm32fxxxxxxx.h
 *             				Ad esempio, quello per il processore STM32F303VCT6 usato per il testing e' il file
 *                 			Driver/CMSIS/ST/STM32F3xx/Include/stm32f3030xc.h
 * @param[in] 		PreemptPriorty	priorita' di pre-emption per la linea IRQn. Deve essere un valore
 * 									compreso tra 0 e 15. Valori bassi indicano priorita' alta.
 * @param[in] 		SubPriority		sub-priorita' per la linea IRQn. Deve essere un valore compreso tra 0 e 15.
 * 									Valori bassi indicano priorita' alta.
 *
 * @code
 * eightDStick_Init_EXTI_v2(	&stick,
 *								GPIOC, GPIO_PIN_6, 		// N
 *								GPIOC, GPIO_PIN_8, 		// S
 *								GPIOC, GPIO_PIN_7, 		// E
 *								GPIOC, GPIO_PIN_9,		// W
 *								EXTI9_5_IRQn,			// IRQn line
 *								15,						// preemption priority
 *								0); 					// sub priority
 * @endcode
 */
void eightDStick_Init_EXTI_v2(	eightDStick_t* stick,
								GPIO_TypeDef* N_Port, uint16_t N_Pin,
								GPIO_TypeDef* S_Port, uint16_t S_Pin,
								GPIO_TypeDef* E_Port, uint16_t E_Pin,
								GPIO_TypeDef* W_Port, uint16_t W_Pin,
								IRQn_Type IRQn,
								uint32_t PreemptPriorty,
								uint32_t SubPriority);

/**
 * @brief Stato dello stick
 */
typedef enum {
	eightDStick_Idle, //!< lo stick e' inattivo, non punta in nessuna direzione
	eightDStick_Active //!< lo stick e' attivo, punta verso una direzione
} eightDStick_Status_t;

/**
 * @brief Permette di verificare che uno stick sia attivo o inattivo
 *
 * @warning Se i pin associati ai segnali di pilotaggio del device non sono correttamente
 * configurati come pin di output, il dispositivo non funzionera' correttamente.
 *
 * @param[in] stick oggeto di tipo eightDStick_t correttamente inizializzato
 *
 * @retval eightDStick_Idle se il dispositivo e' inattivo
 * @retval eightDStick_Active se il device e' attivo
 */
eightDStick_Status_t eightDStick_IsIdle(eightDStick_t* stick);

/**
 * @brief Blocca il programma in attesa che uno stick venga attivato
 *
 * @warning Se i pin associati ai segnali di pilotaggio del device non sono correttamente
 * configurati come pin di output, il dispositivo non funzionera' correttamente.
 *
 * @param[in] stick oggeto di tipo eightDStick_t correttamente inizializzato
 *
 * @code
 * // esempio di interrupt handler
 * void EXTI9_5_IRQHandler(void) {
 * eightDStick_WaitWhileIdle(&stick);
 * direction = eightDStick_GetDirection_NoWait(&stick);
 * switch (direction) {
 * 	...
 * 	}
 * eightDStick_WaitWhileBusy(&stick);
 * eightDStick_ClearEXTI(&stick);
 * }
 * @endcode
 */
void eightDStick_WaitWhileIdle(eightDStick_t* stick);

/**
 * @brief Blocca il programma in attesa che uno stick venga reso inattivo (rilascio dello stick)
 *
 * @warning Se i pin associati ai segnali di pilotaggio del device non sono correttamente
 * configurati come pin di output, il dispositivo non funzionera' correttamente.
 *
 * @param[in] stick oggeto di tipo eightDStick_t correttamente inizializzato
 *
 * @code
 * // esempio di interrupt handler
 * void EXTI9_5_IRQHandler(void) {
 * eightDStick_WaitWhileIdle(&stick);
 * direction = eightDStick_GetDirection_NoWait(&stick);
 * switch (direction) {
 * 	...
 * 	}
 * eightDStick_WaitWhileBusy(&stick);
 * eightDStick_ClearEXTI(&stick);
 * }
 * @endcode

 */
void eightDStick_WaitWhileBusy(eightDStick_t* stick);

/**
 * @brief Attenge che lo stick venga orientato in una direzione, restituendola.
 *
 * @warning Se i pin associati ai segnali di pilotaggio del device non sono correttamente
 * configurati come pin di output, il dispositivo non funzionera' correttamente.
 *
 * @param[in] stick oggeto di tipo eightDStick_t correttamente inizializzato
 *
 * @return eventuale direzione in cui e' puntato lo stick
 * @retval North se lo stick punta a nord
 * @retval NorthWest se lo stick punta a nord-ovest
 * @retval West se lo stick punta a ovest
 * @retval SouthWest se lo stick punta a sud-ovest
 * @retval South se lo stick punta a sud
 * @retval SouthEast se lo stick punta a sud-est
 * @retval East se lo stick punta a est
 * @retval NorthEast se lo stick punta a nord-est
 * @retval NoDirection se lo stick non punta ad una particolare direzione
 *
 * @code
 * eightDStick_Direction_t direction = eightDStick_GetDirection(&stick);
 * HD44780_Clear(&lcd);
 * switch (direction) {
 * 	...
 * }
 * @endcode
 */
eightDStick_Direction_t eightDStick_GetDirection(eightDStick_t* stick);

/**
 * @brief Restituisce la direzione in cui e' orientato uno stick, senza bloccare l'esecuzione.
 *
 * La funzione e' pensata per operare con interrupt abilitati ma puo' essere utilizzata anche
 * qualora questi non siano abilitati.
 *
 * @param[in] stick oggeto di tipo eightDStick_t correttamente inizializzato
 *
 * @return eventuale direzione in cui e' puntato lo stick
 * @retval North se lo stick punta a nord
 * @retval NorthWest se lo stick punta a nord-ovest
 * @retval West se lo stick punta a ovest
 * @retval SouthWest se lo stick punta a sud-ovest
 * @retval South se lo stick punta a sud
 * @retval SouthEast se lo stick punta a sud-est
 * @retval East se lo stick punta a est
 * @retval NorthEast se lo stick punta a nord-est
 * @retval NoDirection se lo stick non punta ad una particolare direzione
 *
 * @code
 * // esempio di interrupt handler
 * void EXTI9_5_IRQHandler(void) {
 * eightDStick_WaitWhileIdle(&stick);
 * HAL_GPIO_WritePin(GPIOE, 	GPIO_PIN_8 | GPIO_PIN_9 | GPIO_PIN_10 | GPIO_PIN_11 |
 * 			 	 	 	 	 	GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15, GPIO_PIN_RESET);
 * direction = eightDStick_GetDirection_NoWait(&stick);
 * switch (direction) {
 * 	case North:
 * 		BSP_LED_On(LED7);
 * 		break;
 * 	case NorthWest:
 * 		BSP_LED_On(LED5);
 * 		break;
 * 	case West:
 * 		BSP_LED_On(LED3);
 * 		break;
 * 	case SouthWest:
 * 		BSP_LED_On(LED4);
 * 		break;
 * 	case South:
 * 		BSP_LED_On(LED6);
 * 		break;
 * 	case SouthEast:
 * 		BSP_LED_On(LED8);
 * 		break;
 * 	case East:
 * 		BSP_LED_On(LED10);
 * 		break;
 * 	case NorthEast:
 * 		BSP_LED_On(LED9);
 * 		break;
 * 	default:
 * 		break;
 * 	}
 * eightDStick_WaitWhileBusy(&stick);
 * HAL_GPIO_WritePin(GPIOE, 	GPIO_PIN_8 | GPIO_PIN_9 | GPIO_PIN_10 | GPIO_PIN_11 |
 * 			 	 	 	 	 	GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15, GPIO_PIN_RESET);
 *
 * eightDStick_ClearEXTI(&stick);
 * }
 * @endcode
 */
eightDStick_Direction_t eightDStick_GetDirection_NoWait(eightDStick_t* stick);

/**
 * @brief Pulisce l'infrastruttura per la gestione delle interrupt dopo averne servita una
 *
 * La funzione permette di resettare i flag usati da uno stick durante gestione delle interrupt.
 *
 * @param[in] stick puntatore all'oggetto eightDStick_t di cui rilevare la direzione
 *
 * @code
 * // esempio di interrupt handler
 * void EXTI9_5_IRQHandler(void) {
 * eightDStick_WaitWhileIdle(&stick);
 * direction = eightDStick_GetDirection_NoWait(&stick);
 * switch (direction) {
 * 	...
 * 	}
 * eightDStick_WaitWhileBusy(&stick);
 * eightDStick_ClearEXTI(&stick);
 * }
 * @endcode
 */
void eightDStick_ClearEXTI(eightDStick_t* stick);


#endif

/**
 * @}
 * @}
 */
