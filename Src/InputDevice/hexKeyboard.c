/**
 * @file hexKeyboard.c
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of STM32DriverPack
 *
 * STM32DriverPack is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * STM32DriverPack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "hexKeyboard.h"
#include <assert.h>
#include <stdlib.h>

// valori assunti dalle linee connesse al keyboard in corrispondenza della pressione di un tasto
const uint8_t keypad_port_value[16] = {
//	A		B		C		D
	0x88,	0x48,	0x28,	0x18,	// 1
	0x84,	0x44,	0x24,	0x14,	// 2
	0x82,	0x42,	0x22,	0x12,	// 3
	0x81,	0x41,	0x21,	0x11	// 4
};

const HexKeypad_Key_t HexKeypad_DefaultCharMap[17] =
{
//  A       B	 	C	 	D
	A1, 	B1, 	C1, 	D1,	// riga 1
	A2, 	B2, 	C2, 	D2,	// riga 2
	A3, 	B3,		C3, 	D3,	// riga 3
	A4, 	B4, 	C4, 	D4,	// riga 4
	NoKey
};

/*================================================================================================
 * Funzioni private del modulo
 *==============================================================================================*/

int HexKeypad_ValidatePair(HexKeypad_t* keypad);

int HexKeypad_ValidatePair_EXTI(HexKeypad_t* keypad);

uint8_t	HexKeypad_GetPortState(HexKeypad_t* keypad);

void HexKeypad_SetRowState(HexKeypad_t* keypad, uint8_t status);

void HexKeypad_ConfigurePin(HexKeypad_t* keypad);

void HexKeypad_ConfigurePin_EXTI(HexKeypad_t* keypad);

/*================================================================================================
 * Implementazione funzioni inizializzazione
 *==============================================================================================*/

 void HexKeypad_Init(	HexKeypad_t* keypad,
						PortPinPair_t ColumnA,
						PortPinPair_t ColumnB,
						PortPinPair_t ColumnC,
						PortPinPair_t ColumnD,
						PortPinPair_t Row1,
						PortPinPair_t Row2,
						PortPinPair_t Row3,
						PortPinPair_t Row4)
{
	assert(keypad);
	keypad->ColumnA.Port = ColumnA.Port;
	keypad->ColumnB.Port = ColumnB.Port;
	keypad->ColumnC.Port = ColumnC.Port;
	keypad->ColumnD.Port = ColumnD.Port;
	keypad->Row1.Port = Row1.Port;
	keypad->Row2.Port = Row2.Port;
	keypad->Row3.Port = Row3.Port;
	keypad->Row4.Port = Row4.Port;
	keypad->ColumnA.Pin = ColumnA.Pin;
	keypad->ColumnB.Pin = ColumnB.Pin;
	keypad->ColumnC.Pin = ColumnC.Pin;
	keypad->ColumnD.Pin = ColumnD.Pin;
	keypad->Row1.Pin = Row1.Pin;
	keypad->Row2.Pin = Row2.Pin;
	keypad->Row3.Pin = Row3.Pin;
	keypad->Row4.Pin = Row4.Pin;
	assert(HexKeypad_ValidatePair(keypad));
	HexKeypad_ConfigurePin(keypad);
}

void HexKeypad_Init_EXTI(	HexKeypad_t* keypad,
										PortPinPair_t ColumnA,
										PortPinPair_t ColumnB,
										PortPinPair_t ColumnC,
										PortPinPair_t ColumnD,
										PortPinPair_t Row1,
										PortPinPair_t Row2,
										PortPinPair_t Row3,
										PortPinPair_t Row4,
										IRQn_Type IRQn,
										uint32_t PreemptPriorty,
										uint32_t SubPriority)
{
	assert(keypad);
	assert(IRQn == EXTI_LINE_9_5 || IRQn == EXTI_LINE_15_10);
	keypad->ColumnA.Port = ColumnA.Port;
	keypad->ColumnB.Port = ColumnB.Port;
	keypad->ColumnC.Port = ColumnC.Port;
	keypad->ColumnD.Port = ColumnD.Port;
	keypad->Row1.Port = Row1.Port;
	keypad->Row2.Port = Row2.Port;
	keypad->Row3.Port = Row3.Port;
	keypad->Row4.Port = Row4.Port;
	keypad->ColumnA.Pin = ColumnA.Pin;
	keypad->ColumnB.Pin = ColumnB.Pin;
	keypad->ColumnC.Pin = ColumnC.Pin;
	keypad->ColumnD.Pin = ColumnD.Pin;
	keypad->Row1.Pin = Row1.Pin;
	keypad->Row2.Pin = Row2.Pin;
	keypad->Row3.Pin = Row3.Pin;
	keypad->Row4.Pin = Row4.Pin;
	assert(HexKeypad_ValidatePair_EXTI(keypad));
	HexKeypad_ConfigurePin_EXTI(keypad);

	keypad->IRQn = IRQn;
	keypad->PreemptPriorty = PreemptPriorty;
	keypad->SubPriority = SubPriority;
	HAL_NVIC_SetPriority(keypad->IRQn, keypad->PreemptPriorty, keypad->SubPriority);
	HAL_NVIC_EnableIRQ(keypad->IRQn);
}

void HexKeypad_Init_v2(	HexKeypad_t* keypad,
						GPIO_TypeDef* ColumnA_Port, uint16_t ColumnA_Pin,
						GPIO_TypeDef* ColumnB_Port, uint16_t ColumnB_Pin,
						GPIO_TypeDef* ColumnC_Port, uint16_t ColumnC_Pin,
						GPIO_TypeDef* ColumnD_Port, uint16_t ColumnD_Pin,
						GPIO_TypeDef* Row1_Port, uint16_t Row1_Pin,
						GPIO_TypeDef* Row2_Port, uint16_t Row2_Pin,
						GPIO_TypeDef* Row3_Port, uint16_t Row3_Pin,
						GPIO_TypeDef* Row4_Port, uint16_t Row4_Pin)
{
	PortPinPair_t ColumnA = {ColumnA_Port, ColumnA_Pin},
	ColumnB = {ColumnB_Port, ColumnB_Pin},
	ColumnC = {ColumnC_Port, ColumnC_Pin},
	ColumnD = {ColumnD_Port, ColumnD_Pin},
	Row1 = {Row1_Port, Row1_Pin},
	Row2 = {Row2_Port, Row2_Pin},
	Row3 = {Row3_Port, Row3_Pin},
	Row4 = {Row4_Port, Row4_Pin};
	HexKeypad_Init(keypad, ColumnA, ColumnB, ColumnC, ColumnD, Row1, Row2, Row3, Row4);
}

void HexKeypad_Init_EXTI_v2(	HexKeypad_t* keypad,
								GPIO_TypeDef* ColumnA_Port, uint16_t ColumnA_Pin,
								GPIO_TypeDef* ColumnB_Port, uint16_t ColumnB_Pin,
								GPIO_TypeDef* ColumnC_Port, uint16_t ColumnC_Pin,
								GPIO_TypeDef* ColumnD_Port, uint16_t ColumnD_Pin,
								GPIO_TypeDef* Row1_Port, uint16_t Row1_Pin,
								GPIO_TypeDef* Row2_Port, uint16_t Row2_Pin,
								GPIO_TypeDef* Row3_Port, uint16_t Row3_Pin,
								GPIO_TypeDef* Row4_Port, uint16_t Row4_Pin,
								IRQn_Type IRQn,
								uint32_t PreemptPriorty,
								uint32_t SubPriority)
{
	PortPinPair_t ColumnA = {ColumnA_Port, ColumnA_Pin},
	ColumnB = {ColumnB_Port, ColumnB_Pin},
	ColumnC = {ColumnC_Port, ColumnC_Pin},
	ColumnD = {ColumnD_Port, ColumnD_Pin},
	Row1 = {Row1_Port, Row1_Pin},
	Row2 = {Row2_Port, Row2_Pin},
	Row3 = {Row3_Port, Row3_Pin},
	Row4 = {Row4_Port, Row4_Pin};

	HexKeypad_Init_EXTI(	keypad,
							ColumnA, ColumnB, ColumnC, ColumnD,
							Row1, Row2, Row3, Row4,
							IRQn, PreemptPriorty, SubPriority);
}

HexKeypad_Status_t HexKeypad_IsIdle(HexKeypad_t* keypad) {
	assert(keypad);
	HexKeypad_Status_t idle = (HexKeypad_GetPortState(keypad) == 0x0F ?  HexKeypad_Idle : HexKeypad_Active);
	return idle;
}

void HexKeypad_WaitWhileIdle(HexKeypad_t* keypad) {
	assert(keypad);
	HexKeypad_SetRowState(keypad, 0x0F);
	while (HexKeypad_GetPortState(keypad) == 0x0F);
}

void HexKeypad_WaitWhileBusy(HexKeypad_t* keypad) {
	assert(keypad);
	HexKeypad_SetRowState(keypad, 0x0F);
	while (HexKeypad_GetPortState(keypad) != 0x0F);
}


HexKeypad_Key_t HexKeypad_GetChar(HexKeypad_t* keypad)
{
	assert(keypad);
	HexKeypad_Key_t character;
	HexKeypad_SetRowState(keypad, 0x0F);
	while (HexKeypad_GetPortState(keypad) == 0x0F);	//polling
	timer_wait_ms(HexKeypad_DebounceWait);

	character = HexKeypad_GetChar_NoWait(keypad);
	while (HexKeypad_GetPortState(keypad) != 0x0F);	//polling
	timer_wait_ms(HexKeypad_DebounceWait);
	return character;
}

HexKeypad_Key_t HexKeypad_GetChar_NoWait(HexKeypad_t* keypad)
{
	assert(keypad);

	// verifico quale tasto sia stato premuto
	char active_value = 0, port = 0x01;
	active_value = HexKeypad_GetPortState(keypad) & 0xF0;
	do {
		HexKeypad_SetRowState(keypad, port);
		if ((HexKeypad_GetPortState(keypad) & 0xF0) == 0x00)
			port = port << 1;
	} while ((HexKeypad_GetPortState(keypad) & 0xF0) == 0x00 && port != 0);
	active_value += port;

	// creo la corrispondenza tasto-carattere
	// se non e' possibile trovare una corrispondenza viene restituito un errore
	uint8_t i = 0;
	while (i < 16 && active_value != keypad_port_value[i])
		i++;
	HexKeypad_SetRowState(keypad, 0x0F);
	return HexKeypad_DefaultCharMap[i];
}

void HexKeypad_ClearEXTI(HexKeypad_t *keypad) {
	__HAL_GPIO_EXTI_CLEAR_IT(keypad->ColumnA.Pin);
	__HAL_GPIO_EXTI_CLEAR_IT(keypad->ColumnB.Pin);
	__HAL_GPIO_EXTI_CLEAR_IT(keypad->ColumnC.Pin);
	__HAL_GPIO_EXTI_CLEAR_IT(keypad->ColumnD.Pin);
	__HAL_GPIO_EXTI_CLEAR_FLAG(keypad->ColumnA.Pin);
	__HAL_GPIO_EXTI_CLEAR_FLAG(keypad->ColumnB.Pin);
	__HAL_GPIO_EXTI_CLEAR_FLAG(keypad->ColumnC.Pin);
	__HAL_GPIO_EXTI_CLEAR_FLAG(keypad->ColumnD.Pin);
	HAL_NVIC_ClearPendingIRQ(keypad->IRQn);
}

int HexKeypad_ValidatePair(HexKeypad_t* keypad)
{
	const int array_dim = 8;
	int i, j;
	const PortPinPair_t pair[] = {
		keypad->ColumnA, keypad->ColumnB, keypad->ColumnC, keypad->ColumnD,
		keypad->Row1, keypad->Row2, keypad->Row3, keypad->Row4};
	for (i = 0; i < array_dim; i++)
		for (j = i+1; j < array_dim; j++)
			if (	pair[i].Port != 0 &&
					pair[j].Port != 0 &&
					pair[i].Port == pair[j].Port &&
					pair[i].Pin == pair[j].Pin)
				return 0;
	return 1;
}

int HexKeypad_ValidatePair_EXTI(HexKeypad_t* keypad)
{
	const int array_dim = 8;
	int i, j;
	const PortPinPair_t pair[] = {
		keypad->ColumnA, keypad->ColumnB, keypad->ColumnC, keypad->ColumnD,
		keypad->Row1, keypad->Row2, keypad->Row3, keypad->Row4};

	// test di disgiunzione delle coppie
	for (i = 0; i < array_dim; i++)
		for (j = i+1; j < array_dim; j++)
			if (	pair[i].Port != 0 &&
					pair[j].Port != 0 &&
					pair[i].Port == pair[j].Port &&
					pair[i].Pin == pair[j].Pin)
				return 0;

	// test di compatibilita' con la linea EXTI 9_5
	if(keypad->IRQn == EXTI_LINE_9_5)
		for (i = 0; i < (array_dim >> 1); i++)
			if (
					pair[i].Pin != GPIO_PIN_5 &&
					pair[i].Pin != GPIO_PIN_6 &&
					pair[i].Pin != GPIO_PIN_7 &&
					pair[i].Pin != GPIO_PIN_8 &&
					pair[i].Pin != GPIO_PIN_9
			)
				return 0;

	// test di compatibilita' con la linea EXTI 15_10
	if (keypad->IRQn == EXTI_LINE_15_10)
		for (i = 0; i < (array_dim >> 1); i++)
			if (
					pair[i].Pin != GPIO_PIN_10 &&
					pair[i].Pin != GPIO_PIN_11 &&
					pair[i].Pin != GPIO_PIN_12 &&
					pair[i].Pin != GPIO_PIN_13 &&
					pair[i].Pin != GPIO_PIN_14 &&
					pair[i].Pin != GPIO_PIN_15
			)
				return 0;
	return 1;
}

uint8_t	HexKeypad_GetPortState(HexKeypad_t* keypad)
{
	uint8_t status = 0;
	// bit 7 : Column A
	// bit 6 : Column B
	// bit 5 : Column C
	// bit 4 : Column D
	// bit 3 : Row 4
	// bit 2 : Row 3
	// bit 1 : Row 2
	// bit 0 : Row 1
	status += (HAL_GPIO_ReadPin(keypad->ColumnA.Port, keypad->ColumnA.Pin) == GPIO_PIN_SET ? 0x80 : 0x00);
	status += (HAL_GPIO_ReadPin(keypad->ColumnB.Port, keypad->ColumnB.Pin) == GPIO_PIN_SET ? 0x40 : 0x00);
	status += (HAL_GPIO_ReadPin(keypad->ColumnC.Port, keypad->ColumnC.Pin) == GPIO_PIN_SET ? 0x20 : 0x00);
	status += (HAL_GPIO_ReadPin(keypad->ColumnD.Port, keypad->ColumnD.Pin) == GPIO_PIN_SET ? 0x10 : 0x00);
	status += (HAL_GPIO_ReadPin(keypad->Row4.Port, keypad->Row4.Pin) == GPIO_PIN_SET ? 0x08 : 0x00);
	status += (HAL_GPIO_ReadPin(keypad->Row3.Port, keypad->Row3.Pin) == GPIO_PIN_SET ? 0x04 : 0x00);
	status += (HAL_GPIO_ReadPin(keypad->Row2.Port, keypad->Row2.Pin) == GPIO_PIN_SET ? 0x02 : 0x00);
	status += (HAL_GPIO_ReadPin(keypad->Row1.Port, keypad->Row1.Pin) == GPIO_PIN_SET ? 0x01 : 0x00);
	return status;
}

void HexKeypad_SetRowState(HexKeypad_t* keypad, uint8_t status)
{
	HAL_GPIO_WritePin(keypad->Row4.Port, keypad->Row4.Pin, (status & 0x08) == 0 ? GPIO_PIN_RESET : GPIO_PIN_SET);
	HAL_GPIO_WritePin(keypad->Row3.Port, keypad->Row3.Pin, (status & 0x04) == 0 ? GPIO_PIN_RESET : GPIO_PIN_SET);
	HAL_GPIO_WritePin(keypad->Row2.Port, keypad->Row2.Pin, (status & 0x02) == 0 ? GPIO_PIN_RESET : GPIO_PIN_SET);
	HAL_GPIO_WritePin(keypad->Row1.Port, keypad->Row1.Pin, (status & 0x01) == 0 ? GPIO_PIN_RESET : GPIO_PIN_SET);
}

void HexKeypad_ConfigurePin(HexKeypad_t* keypad)
{
	const int array_dim = 4;
	int i;
	const PortPinPair_t columns[] = {keypad->ColumnA, keypad->ColumnB, keypad->ColumnC, keypad->ColumnD};
	const PortPinPair_t rows[] = {keypad->Row1, keypad->Row2, keypad->Row3, keypad->Row4};

	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	for (i = 0; i < array_dim; i++)
	{
		GPIO_InitStruct.Pin = rows[i].Pin;
		HAL_GPIO_Init(rows[i].Port, &GPIO_InitStruct);
		HAL_GPIO_WritePin(rows[i].Port, rows[i].Pin, GPIO_PIN_RESET);
	}

	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	for (i = 0; i < array_dim; i++)
	{
		GPIO_InitStruct.Pin = columns[i].Pin;
		HAL_GPIO_Init(columns[i].Port, &GPIO_InitStruct);
	}

	HexKeypad_SetRowState(keypad, 0x0F);
}

void HexKeypad_ConfigurePin_EXTI(HexKeypad_t* keypad)
{
	const int array_dim = 4;
	int i;
	const PortPinPair_t columns[] = {keypad->ColumnA, keypad->ColumnB, keypad->ColumnC, keypad->ColumnD};
	const PortPinPair_t rows[] = {keypad->Row1, keypad->Row2, keypad->Row3, keypad->Row4};

	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	for (i = 0; i < array_dim; i++)
	{
		GPIO_InitStruct.Pin = rows[i].Pin;
		HAL_GPIO_Init(rows[i].Port, &GPIO_InitStruct);
		HAL_GPIO_WritePin(rows[i].Port, rows[i].Pin, GPIO_PIN_RESET);
	}

	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	for (i = 0; i < array_dim; i++)
	{
		GPIO_InitStruct.Pin = columns[i].Pin;
		HAL_GPIO_Init(columns[i].Port, &GPIO_InitStruct);
	}

	HexKeypad_SetRowState(keypad, 0x0F);
}
