var _p_w_motor_8h =
[
    [ "PWMotor_Ch_t", "group___p_w_motor.html#ga1eeaebb2a9be6bdbb92347ec9f769fa2", [
      [ "PWM_ch_1", "group___p_w_motor.html#gga1eeaebb2a9be6bdbb92347ec9f769fa2a16ef41400517d629609893ffe35c5a3a", null ],
      [ "PWM_ch_2", "group___p_w_motor.html#gga1eeaebb2a9be6bdbb92347ec9f769fa2acb1ee5604ab5ac4fe074011e697b36cf", null ],
      [ "PWM_ch_3", "group___p_w_motor.html#gga1eeaebb2a9be6bdbb92347ec9f769fa2aac8b66d2d7caf1cba1236480c5673440", null ],
      [ "PWM_ch_4", "group___p_w_motor.html#gga1eeaebb2a9be6bdbb92347ec9f769fa2a8541e95131bad83cf215c3f2cae4017a", null ]
    ] ],
    [ "PWMotor_Direction_t", "group___p_w_motor.html#ga0761ef22143f59318f529e5462455fe6", [
      [ "PWMotor_forward", "group___p_w_motor.html#gga0761ef22143f59318f529e5462455fe6ac3359916ab4a0d6c1946bce22042b115", null ],
      [ "PWMotor_reverse", "group___p_w_motor.html#gga0761ef22143f59318f529e5462455fe6a80770105c3b73db43257b2592502074b", null ]
    ] ],
    [ "PWMotor_Polarity_t", "group___p_w_motor.html#ga18ecd2c6298fd00c3f5c247df5ad12a6", [
      [ "polarity_low", "group___p_w_motor.html#gga18ecd2c6298fd00c3f5c247df5ad12a6ad908d6026b01104eb046dc9dcf545a24", null ],
      [ "polarity_high", "group___p_w_motor.html#gga18ecd2c6298fd00c3f5c247df5ad12a6a1d3b592d50a8d8e3ca836724fd5c7896", null ]
    ] ],
    [ "PWMotor_TIM_t", "group___p_w_motor.html#ga8b11e2f09702d21324941971cba7fd6b", [
      [ "PWM_2", "group___p_w_motor.html#gga8b11e2f09702d21324941971cba7fd6bab8296af41c4fdd6fd2dc62d6c761bdf6", null ],
      [ "PWM_3", "group___p_w_motor.html#gga8b11e2f09702d21324941971cba7fd6ba44646d1313a5c911c1500447f531f542", null ],
      [ "PWM_4", "group___p_w_motor.html#gga8b11e2f09702d21324941971cba7fd6bae05454e483b89fa5573dc4846c991dfa", null ]
    ] ],
    [ "PWMotor_Init", "group___p_w_motor.html#ga6527c16d9de56574df1c7a487b935974", null ],
    [ "PWMotor_SetDirection", "group___p_w_motor.html#ga50d37a81ec029cb5210fc3b8eabd1aca", null ],
    [ "PWMotor_SetDutyCycle", "group___p_w_motor.html#ga1509c148153b427e52a09fcf146ebe53", null ],
    [ "PWMotor_SetFrequency", "group___p_w_motor.html#ga93aa4aae28ecb5e42b74b1569f65ec7b", null ],
    [ "PWMotor_Start", "group___p_w_motor.html#ga3e1b08587e85aed94826f021559448c1", null ],
    [ "PWMotor_Stop", "group___p_w_motor.html#ga611ac32c174f0acdbf40c2d8a0e4ec51", null ],
    [ "PWMotor_ToggleDirection", "group___p_w_motor.html#ga40582a26e9d52958d07d155a4b996299", null ]
];