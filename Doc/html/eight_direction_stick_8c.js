var eight_direction_stick_8c =
[
    [ "eightDStick_ClearEXTI", "group__eight_d_stick.html#gaa8537a4ad9f0a06a6eec73f6b77f1bfa", null ],
    [ "eightDStick_ConfigurePin", "eight_direction_stick_8c.html#a112844b2a7e9b198a3b953e96b7d9041", null ],
    [ "eightDStick_ConfigurePin_EXTI", "eight_direction_stick_8c.html#a9e54bb2f8e9796f30f5ccd88ad3607b1", null ],
    [ "eightDStick_GetDirection", "group__eight_d_stick.html#ga4212ba1bd3106a466b7cce4293ab7d1b", null ],
    [ "eightDStick_GetDirection_NoWait", "group__eight_d_stick.html#ga73b5012104630b3be78274c97fede588", null ],
    [ "eightDStick_GetPortState", "eight_direction_stick_8c.html#a886d9ef2cd5790e0c519b3d1056bd743", null ],
    [ "eightDStick_Init", "group__eight_d_stick.html#gad4b6f723fafa684db2dd346d235acaca", null ],
    [ "eightDStick_Init_EXTI", "group__eight_d_stick.html#gab1fc6fc3e439b4ab1e90ceb58328a82e", null ],
    [ "eightDStick_Init_EXTI_v2", "group__eight_d_stick.html#gae2eaf2afe9677ad4629ea2e2610e2d79", null ],
    [ "eightDStick_Init_v2", "group__eight_d_stick.html#ga8050070bfd8c736ca3e3be03a7d6d590", null ],
    [ "eightDStick_IsIdle", "group__eight_d_stick.html#gaf86a8a586ca8144787b7a3757cfb5cd3", null ],
    [ "eightDStick_ValidatePair", "eight_direction_stick_8c.html#a5987432151542e17d16eb22efbe63e86", null ],
    [ "eightDStick_ValidatePair_EXTI", "eight_direction_stick_8c.html#a586ff680c12eb5c5348923ee945c7e2f", null ],
    [ "eightDStick_WaitWhileBusy", "group__eight_d_stick.html#ga01a1caf1f20307b1cfa584243d27a7f5", null ],
    [ "eightDStick_WaitWhileIdle", "group__eight_d_stick.html#ga5b450f97c40e333d9c6188397ba9048c", null ]
];