var group__eight_d_stick =
[
    [ "eightDStick_t", "structeight_d_stick__t.html", [
      [ "E", "structeight_d_stick__t.html#adc40b43010297aa8e44f3722eb927bed", null ],
      [ "IRQn", "structeight_d_stick__t.html#a9534b79321707b92e9f5287d945a4b9e", null ],
      [ "N", "structeight_d_stick__t.html#a6f7c14a96af5f06b316ffaab10a57335", null ],
      [ "PreemptPriorty", "structeight_d_stick__t.html#af6ab8c6eec76621e291746a357c3bb4e", null ],
      [ "S", "structeight_d_stick__t.html#aa25a6d5ddd572f2eee166ff00226cf1c", null ],
      [ "SubPriority", "structeight_d_stick__t.html#aa449275ca2a54b383bcc6100a0eeae64", null ],
      [ "W", "structeight_d_stick__t.html#a1f3bc28c0910fe5abbc4c01a95ead1c9", null ]
    ] ],
    [ "eightDStick_DebounceWait", "group__eight_d_stick.html#ga4f73ec5581b1930644d836803c4615f2", null ],
    [ "eightDStick_Direction_t", "group__eight_d_stick.html#ga96f7a3a8683a15df1f59b7f88b4cf79d", [
      [ "North", "group__eight_d_stick.html#gga96f7a3a8683a15df1f59b7f88b4cf79daaeb9634ef4d4fd4a5297ada06f89c3fe", null ],
      [ "NorthWest", "group__eight_d_stick.html#gga96f7a3a8683a15df1f59b7f88b4cf79dad3768262dddc98c8e37f4a7c20496ab4", null ],
      [ "West", "group__eight_d_stick.html#gga96f7a3a8683a15df1f59b7f88b4cf79daedd15c5b13ad5525c46d5c9650ddde5f", null ],
      [ "SouthWest", "group__eight_d_stick.html#gga96f7a3a8683a15df1f59b7f88b4cf79da1154cc523fb20e3db29f51f9156ce6aa", null ],
      [ "South", "group__eight_d_stick.html#gga96f7a3a8683a15df1f59b7f88b4cf79dac15660611b40cfc69e36003c8607311a", null ],
      [ "SouthEast", "group__eight_d_stick.html#gga96f7a3a8683a15df1f59b7f88b4cf79da5170d8d1f4d6ebe3d8c43f545e260d4a", null ],
      [ "East", "group__eight_d_stick.html#gga96f7a3a8683a15df1f59b7f88b4cf79da66183dce5a3b8d48fbdfe1f8ede7fcc2", null ],
      [ "NorthEast", "group__eight_d_stick.html#gga96f7a3a8683a15df1f59b7f88b4cf79da1e7b90df2366909c5e445f790e167872", null ],
      [ "NoDirection", "group__eight_d_stick.html#gga96f7a3a8683a15df1f59b7f88b4cf79daca3e0efea800bf6b107cc8fa09e4bb06", null ]
    ] ],
    [ "eightDStick_Status_t", "group__eight_d_stick.html#ga5b73dee6410bdff53422ad2a5add0986", [
      [ "eightDStick_Idle", "group__eight_d_stick.html#gga5b73dee6410bdff53422ad2a5add0986ae5795e65624d9f937ad53598761271df", null ],
      [ "eightDStick_Active", "group__eight_d_stick.html#gga5b73dee6410bdff53422ad2a5add0986a75d53d1c3c9f88d328fc8f03a2277380", null ]
    ] ],
    [ "eightDStick_ClearEXTI", "group__eight_d_stick.html#gaa8537a4ad9f0a06a6eec73f6b77f1bfa", null ],
    [ "eightDStick_GetDirection", "group__eight_d_stick.html#ga4212ba1bd3106a466b7cce4293ab7d1b", null ],
    [ "eightDStick_GetDirection_NoWait", "group__eight_d_stick.html#ga73b5012104630b3be78274c97fede588", null ],
    [ "eightDStick_Init", "group__eight_d_stick.html#gad4b6f723fafa684db2dd346d235acaca", null ],
    [ "eightDStick_Init_EXTI", "group__eight_d_stick.html#gab1fc6fc3e439b4ab1e90ceb58328a82e", null ],
    [ "eightDStick_Init_EXTI_v2", "group__eight_d_stick.html#gae2eaf2afe9677ad4629ea2e2610e2d79", null ],
    [ "eightDStick_Init_v2", "group__eight_d_stick.html#ga8050070bfd8c736ca3e3be03a7d6d590", null ],
    [ "eightDStick_IsIdle", "group__eight_d_stick.html#gaf86a8a586ca8144787b7a3757cfb5cd3", null ],
    [ "eightDStick_WaitWhileBusy", "group__eight_d_stick.html#ga01a1caf1f20307b1cfa584243d27a7f5", null ],
    [ "eightDStick_WaitWhileIdle", "group__eight_d_stick.html#ga5b450f97c40e333d9c6188397ba9048c", null ]
];