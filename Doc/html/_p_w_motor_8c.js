var _p_w_motor_8c =
[
    [ "PWMotor_Init", "group___p_w_motor.html#ga6527c16d9de56574df1c7a487b935974", null ],
    [ "PWMotor_SetDirection", "group___p_w_motor.html#ga50d37a81ec029cb5210fc3b8eabd1aca", null ],
    [ "PWMotor_SetDutyCycle", "group___p_w_motor.html#ga1509c148153b427e52a09fcf146ebe53", null ],
    [ "PWMotor_SetFrequency", "group___p_w_motor.html#ga93aa4aae28ecb5e42b74b1569f65ec7b", null ],
    [ "PWMotor_Start", "group___p_w_motor.html#ga3e1b08587e85aed94826f021559448c1", null ],
    [ "PWMotor_Stop", "group___p_w_motor.html#ga611ac32c174f0acdbf40c2d8a0e4ec51", null ],
    [ "PWMotor_ToggleDirection", "group___p_w_motor.html#ga40582a26e9d52958d07d155a4b996299", null ],
    [ "PWMotor_GPIO_Alternate", "_p_w_motor_8c.html#a1b663ea04e0f739d75d4867106016459", null ],
    [ "PWMotor_GPIO_PortPair", "_p_w_motor_8c.html#af49cd8ee191ea75e019169278a079582", null ],
    [ "PWMotor_TIM_BaseAddr", "_p_w_motor_8c.html#aa86b95edfbe653192a4f04ebd149daec", null ],
    [ "PWMotor_TIM_Ch", "_p_w_motor_8c.html#a57171b9af80cc6bfe1eec81a41e7bd51", null ]
];