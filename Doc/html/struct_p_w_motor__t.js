var struct_p_w_motor__t =
[
    [ "duty_cycle", "struct_p_w_motor__t.html#a40f1edd17401f3f630cefd0033e50354", null ],
    [ "frequency", "struct_p_w_motor__t.html#ab632fb0b4d5156ea4df0b1e15410e913", null ],
    [ "gpio_dir", "struct_p_w_motor__t.html#a760dec241466f73a833e5915f46c4b37", null ],
    [ "polarity", "struct_p_w_motor__t.html#a6e6c535f41956a65ed7a5362791b7de9", null ],
    [ "tim", "struct_p_w_motor__t.html#ad9085dabe4e03632d3d6cde91272cdf8", null ],
    [ "TIMhandle", "struct_p_w_motor__t.html#a2e29e6fe1bf6d10fbd230fec779be06b", null ]
];