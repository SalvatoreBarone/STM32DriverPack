var group___hex_keyboard =
[
    [ "HexKeypad_t", "struct_hex_keypad__t.html", [
      [ "ColumnA", "struct_hex_keypad__t.html#af06650fc5c182955f9bb16d2d55ad5d5", null ],
      [ "ColumnB", "struct_hex_keypad__t.html#a640f6d29a89c26b1a75468965eba298d", null ],
      [ "ColumnC", "struct_hex_keypad__t.html#a97ddf046c5d0e95411984034950ebf33", null ],
      [ "ColumnD", "struct_hex_keypad__t.html#a140101c301ac2864a979fe6cd4e33d90", null ],
      [ "IRQn", "struct_hex_keypad__t.html#a9534b79321707b92e9f5287d945a4b9e", null ],
      [ "PreemptPriorty", "struct_hex_keypad__t.html#af6ab8c6eec76621e291746a357c3bb4e", null ],
      [ "Row1", "struct_hex_keypad__t.html#ad34015605706affe4d80c2cf1207af5b", null ],
      [ "Row2", "struct_hex_keypad__t.html#a25a2e5340fa3c3b522c93e5dcc3b45f3", null ],
      [ "Row3", "struct_hex_keypad__t.html#afb08d68aef579ec479641852842844cc", null ],
      [ "Row4", "struct_hex_keypad__t.html#a2e735987e201fbaa13c1f1e81a93d2fd", null ],
      [ "SubPriority", "struct_hex_keypad__t.html#aa449275ca2a54b383bcc6100a0eeae64", null ]
    ] ],
    [ "HexKeypad_DebounceWait", "group___hex_keyboard.html#ga0fd218b9aeab869c6cdd3ee2fc0fd8da", null ],
    [ "HexKeypad_Key_t", "group___hex_keyboard.html#gae577557a60fdd285ebf5e6eb9c994cb7", null ],
    [ "HexKeypad_Status_t", "group___hex_keyboard.html#gad9954b383c636187a7464ec66fd340d9", [
      [ "HexKeypad_Idle", "group___hex_keyboard.html#ggad9954b383c636187a7464ec66fd340d9a38dab01df2ab4df2c1af1ad2c0d222a9", null ],
      [ "HexKeypad_Active", "group___hex_keyboard.html#ggad9954b383c636187a7464ec66fd340d9aa0f9042bac3ed5aa839d39dbe0356ebe", null ]
    ] ],
    [ "HexKeypad_ClearEXTI", "group___hex_keyboard.html#ga4f3a06ca5069b75b61c28ad57034a864", null ],
    [ "HexKeypad_GetChar", "group___hex_keyboard.html#gaaba046ae447cb952f89f6dcf9cd023ed", null ],
    [ "HexKeypad_GetChar_NoWait", "group___hex_keyboard.html#ga5d1f7098d2e2bbd7b77444666f01c50c", null ],
    [ "HexKeypad_Init", "group___hex_keyboard.html#gaed751179a1cd142be05b52314fff6979", null ],
    [ "HexKeypad_Init_EXTI", "group___hex_keyboard.html#ga262a1cf492c4e303cf3ada6d507404d3", null ],
    [ "HexKeypad_Init_EXTI_v2", "group___hex_keyboard.html#ga0826dad90e92f2f51643fc7151f2167c", null ],
    [ "HexKeypad_Init_v2", "group___hex_keyboard.html#ga16c9b2fbf2bd7f376c8d83c0491657c4", null ],
    [ "HexKeypad_IsIdle", "group___hex_keyboard.html#ga53ea0db89a7e9d8d6a134444bb978b97", null ],
    [ "HexKeypad_WaitWhileBusy", "group___hex_keyboard.html#ga90cd9522c7118ef1c3ff650584126881", null ],
    [ "HexKeypad_WaitWhileIdle", "group___hex_keyboard.html#ga9ee25472002e1b4eaa2380266c62bff6", null ]
];