var hex_keyboard_8h =
[
    [ "HexKeypad_DebounceWait", "group___hex_keyboard.html#ga0fd218b9aeab869c6cdd3ee2fc0fd8da", null ],
    [ "HexKeypad_Key_t", "group___hex_keyboard.html#gae577557a60fdd285ebf5e6eb9c994cb7", [
      [ "A1", "group___hex_keyboard.html#ggae577557a60fdd285ebf5e6eb9c994cb7a6ff26890857c886c86453f0c8078bf95", null ],
      [ "B1", "group___hex_keyboard.html#ggae577557a60fdd285ebf5e6eb9c994cb7ad90c5e7444630b8d170b08d0853aae21", null ],
      [ "C1", "group___hex_keyboard.html#ggae577557a60fdd285ebf5e6eb9c994cb7ae54c31a855b907f263d49edcdbe677bd", null ],
      [ "D1", "group___hex_keyboard.html#ggae577557a60fdd285ebf5e6eb9c994cb7a29b8ecb29049f38cbf752d95f479bff7", null ],
      [ "A2", "group___hex_keyboard.html#ggae577557a60fdd285ebf5e6eb9c994cb7a47329f455692c2a8284d7594405f16d4", null ],
      [ "B2", "group___hex_keyboard.html#ggae577557a60fdd285ebf5e6eb9c994cb7a9bfbe74b27169e5bd5bd5dc020fdd00b", null ],
      [ "C2", "group___hex_keyboard.html#ggae577557a60fdd285ebf5e6eb9c994cb7a5e602f1d68586231698bda7be6af7d2e", null ],
      [ "D2", "group___hex_keyboard.html#ggae577557a60fdd285ebf5e6eb9c994cb7a86c69dc8849d17673b52b9a8d94d8b9f", null ],
      [ "A3", "group___hex_keyboard.html#ggae577557a60fdd285ebf5e6eb9c994cb7a47dbfcefc8fabadcd82806b21de14bfc", null ],
      [ "B3", "group___hex_keyboard.html#ggae577557a60fdd285ebf5e6eb9c994cb7a008e5845e069ecd71e49f3d18ec21130", null ],
      [ "C3", "group___hex_keyboard.html#ggae577557a60fdd285ebf5e6eb9c994cb7ada966660d95922946f59862d9ce54b1c", null ],
      [ "D3", "group___hex_keyboard.html#ggae577557a60fdd285ebf5e6eb9c994cb7ade8ef7573c5fa770f07ac7616cbf5d34", null ],
      [ "A4", "group___hex_keyboard.html#ggae577557a60fdd285ebf5e6eb9c994cb7ada06582608e19a3f2438f54eeb0bcad5", null ],
      [ "B4", "group___hex_keyboard.html#ggae577557a60fdd285ebf5e6eb9c994cb7a6e242c85f0b91e8879200ec3004a4cab", null ],
      [ "C4", "group___hex_keyboard.html#ggae577557a60fdd285ebf5e6eb9c994cb7a24727389909cb6406ed9483df7810c78", null ],
      [ "D4", "group___hex_keyboard.html#ggae577557a60fdd285ebf5e6eb9c994cb7a12761dd9f3b74590b720d87d6ca9fbcf", null ],
      [ "NoKey", "group___hex_keyboard.html#ggae577557a60fdd285ebf5e6eb9c994cb7a101f5c3557c0c90f333b30c9469831c8", null ]
    ] ],
    [ "HexKeypad_Status_t", "group___hex_keyboard.html#gad9954b383c636187a7464ec66fd340d9", [
      [ "HexKeypad_Idle", "group___hex_keyboard.html#ggad9954b383c636187a7464ec66fd340d9a38dab01df2ab4df2c1af1ad2c0d222a9", null ],
      [ "HexKeypad_Active", "group___hex_keyboard.html#ggad9954b383c636187a7464ec66fd340d9aa0f9042bac3ed5aa839d39dbe0356ebe", null ]
    ] ],
    [ "HexKeypad_ClearEXTI", "group___hex_keyboard.html#ga4f3a06ca5069b75b61c28ad57034a864", null ],
    [ "HexKeypad_GetChar", "group___hex_keyboard.html#gaaba046ae447cb952f89f6dcf9cd023ed", null ],
    [ "HexKeypad_GetChar_NoWait", "group___hex_keyboard.html#ga5d1f7098d2e2bbd7b77444666f01c50c", null ],
    [ "HexKeypad_Init", "group___hex_keyboard.html#gaed751179a1cd142be05b52314fff6979", null ],
    [ "HexKeypad_Init_EXTI", "group___hex_keyboard.html#ga262a1cf492c4e303cf3ada6d507404d3", null ],
    [ "HexKeypad_Init_EXTI_v2", "group___hex_keyboard.html#ga0826dad90e92f2f51643fc7151f2167c", null ],
    [ "HexKeypad_Init_v2", "group___hex_keyboard.html#ga16c9b2fbf2bd7f376c8d83c0491657c4", null ],
    [ "HexKeypad_IsIdle", "group___hex_keyboard.html#ga53ea0db89a7e9d8d6a134444bb978b97", null ],
    [ "HexKeypad_WaitWhileBusy", "group___hex_keyboard.html#ga90cd9522c7118ef1c3ff650584126881", null ],
    [ "HexKeypad_WaitWhileIdle", "group___hex_keyboard.html#ga9ee25472002e1b4eaa2380266c62bff6", null ]
];