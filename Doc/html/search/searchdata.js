var indexSectionsWithContent =
{
  0: "cdefghilnprstw",
  1: "ehp",
  2: "cehp",
  3: "deghp",
  4: "cdefginprstw",
  5: "ehp",
  6: "ehnpsw",
  7: "cehilp"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "groups"
};

var indexSectionLabels =
{
  0: "Tutto",
  1: "Strutture dati",
  2: "File",
  3: "Funzioni",
  4: "Variabili",
  5: "Tipi enumerati (enum)",
  6: "Valori del tipo enumerato",
  7: "Gruppi"
};

